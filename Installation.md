[Retour à la page d'accueil](README.md)

# Installation

<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>


> <span style="color : red">**Avant propos:**</span> Ce tutoriel a été réalisé avec la version 11 de Java. JOGL semble connaître des difficultés avec les versions récentes de Java au dessus de Java 16. Il connaît aussi des problèmes avec certaines version avec l'OpenJDK, il est donc fortement conseillé d'utiliser le [JDK d'Oracle](https://www.oracle.com/fr/java/technologies/downloads/) Pour les utilisateurs Linux, nous proposons 2 gammes de solutions en fin d'installation.


## Récupération des fichiers



Après avoir installé Git et Java, il faut rappatrier l'ensemble du projet du site officiel ([https://gitlab.com/jerboateam/jerboa-tuto](https://gitlab.com/jerboateam/jerboa-tuto)) sur votre ordinateur. Pour cela, mettez vous dans un répertoire où vous voulez rapatrier le projet et taper la ligne de commande suivante:

```
git clone https://gitlab.com/jerboateam/jerboa-tuto.git
```

> <span style="color : green">**Remarque:**</span> vous pouvez utiliser un outil graphique pour rapatrier le projet.

Lorsque vous avez rapatrié le projet, vous devez choisir l'environnement dans le lequel vous souhaitez développer:
* [Installation en ligne avec Eclipse](#installation-sous-eclipse)
* [Installation en ligne avec Visual Studio Code](#installation-sous-visual-studio-code)

-----------------------------------------------------------------------------
### Installation sous Eclipse


L'installation sous Eclipse consiste à ajouter un projet Maven existant et d'indiquer le répertoire ``JerboaTuto`` (ce répertoire contient le fichier ``pom.xml``). Aidez-vous des images ci-dessous qui reprend les étapes clés de l'import de projet:

<!-- <figure>
  <img src="images/install_eclipse_step0.png" alt="Etape 0" style="width: 50%;">
  <figcaption>Lancez Eclipse</figcaption>
</figure>
<figure>
  <img src="images/install_eclipse_step1.png" alt="Etape 1" style="width: 50%;">
  <figcaption>Importez un projet existant</figcaption>
</figure> -->

<div style="display: flex; flex-direction: row; align-items: center;">
  <div style="flex: 1;">
    <img src="images/install_eclipse_step0.png" alt="Etape 0" style="max-width: 100%; height: auto;">
    <p>Lancez Eclipse</p>
  </div>
  <div style="flex: 0 0 20px;"></div> <!-- Espace de 20 pixels entre les images -->
  <div style="flex: 1;">
    <img src="images/install_eclipse_step1.png" alt="Etape 1" style="max-width: 100%; height: auto;">
    <p>Importez un projet existant</p>
  </div>
</div>

<div style="display: flex; flex-direction: row; align-items: center;">
  <div style="flex: 1;">
    <img src="images/install_eclipse_step2.png" style="max-width: 100%; height: auto;">
    <p>Choississez Maven > Existing Maven Projects </p>
  </div>
  <div style="flex: 0 0 20px;"></div> <!-- Espace de 20 pixels entre les images -->
  <div style="flex: 1;">
    <img src="images/install_eclipse_step3.png"  style="max-width: 100%; height: auto;">
    <p>Dans la nouvelle fenêtre cliquez sur Browse</p>
  </div>
</div>

<div style="display: flex; flex-direction: row; align-items: center;">
  <div style="flex: 1;">
    <img src="images/install_eclipse_step4.png" style="max-width: 100%; height: auto;">
    <p>Cherchez le dossier <b>JerboaTuto</b> du dépôt comme sur la figure </p>
  </div>
  <div style="flex: 0 0 20px;"></div> <!-- Espace de 20 pixels entre les images -->
  <div style="flex: 1;">
    <img src="images/install_eclipse_step5.png"  style="max-width: 100%; height: auto;">
    <p>Le projet doit doit apparaitre, cliquez sur  Finish</p>
  </div>
</div>

Une fois que le projet est créé (et donc visible dans l'explorateur de projet sous Eclipse), il est conseillé de lancer une mise à jour du projet (normalement Eclipse a déjà fait ce travail automatiquement). Ainsi, Maven va récupérer l'ensemble des dépendances pour le projet (jogl, gluegen, jerboa, jerboa-viewer, ...).
Aidez-vous des images ci-dessous qui reprend les étapes clés de la mise à jour du projet:

<p style="text-align : center">
<img src="images/install_eclipse_step6.png" />
</p>


Si les étapes précédentes ont bien été réalisées, il ne manque plus que lancer l'application pour visualiser le modeleur que vous aller modifier durant ce tuto. Pour cela, vous aller devoir lancer dans eclipse la classe ``jerboa.tuto.launcher.JTLauncher``, pour cela nous allons demander à Eclipse d'exécuter la fonction ``main`` sur cette classe. Procédez comme sur l'image suivante pour lancer l'application:

<p style="text-align : center">
<img src="images/install_eclipse_step7.png" />
</p>

#### Problème de driver - solution 1

> <span style="color : red" >ATTENTION Utilisateur Linux:</span> Il se peut que la version de JOGL ne fonctionne pas sur votre machine en raison d'un problème de driver vidéo et/ou d'environnement, ce problème a été rencontré sous Linux sans drivers NVIDIA actuellement. <br/><br/>
> Cependant, si le problème provient d'un index à -1 (exception de type ``IndexOutOfBoundsException``). Il faut ajouter les arguments suivant à la JVM: <br/>
> ```--add-exports java.base/java.lang=ALL-UNNAMED  --add-exports java.desktop/sun.awt=ALL-UNNAMED --add-exports java.desktop/sun.java2d=ALL-UNNAMED```

#### Problème de driver - solution 2

> <span style="color : red" >ATTENTION Utilisateur Linux:</span> Pour une machine sous Linux sans carte graphique dédiée avec chipset Intel (i.e., Intel(R) HD Graphics), il est possible que la carte ne supporte pas OpenGL Core.<br/><br/>
> L'erreur rencontrée correspond alors à ``Profile GL4bc is not available on X11GraphicsDevice``. Il faut alors ajouter l'argument suivant à la JVM pour désactiver OpenGL Core:<br/>
> ```-Djogl.disable.openglcore=false```

-----------------------------------------------------------------------------
### Installation sous Visual Studio Code

Une fois Visual Studio Code (vscode) lancé, vous devez ouvrir le répertoire JerboaTuto (celui qui contient le fichier ``pom.xml``). L'outil devrait détecter automatiquement le projet ``maven`` en proposant l'installation des extensions manquantes, comme vous le voyez en bas à droite de la capture suivante:

![](images/jerboatuto_vscode_openrep.png "Regardez en bas à droite la détection par VSCode du projet Java/Maven")


Idéalement vous devez installer l'extension nommée _Extension Pack for Java_ comme sur la figure ci-après pour préparer votre environnement.

![](images/jerboatuto_vscode_installenv.png)


L'image ci-après montre l'état de vscode lorsque le projet est entièrement chargé. En particulier, la section ``maven`` contient l'objectif _jerboa-tuto_ avec la possibilité de lancer toutes les commandes nécessaire.

> Rassurez-vous, dans un fonctionnement simple, vscode appelera automatiquement les bonnes commandes pour vous

![](images/jerboatuto_vscode_endload.png "Fin du chargement avec la reconnaissance du code Java et du projet Maven")

Cherchez dans la partie projet Java, la classe ``jerboa.tuto.launcher.JTLauncher`` comme sur la figure ci-desssous et réalisez un clique droit dessus pour développer le menu contextuel.
Enfin, lancez la commande ``Run Java`` pour lancer l'application.

<p style="text-align : center">
<img src="images/jerboatuto_vscode_runapp.png" />
</p>

> <span style="color : red;">**ATTENTION:**</span> 
Si l'application se lance, alors vous avez terminé l'installation, si vous avez des erreurs, vous pouvez continuer votre lecture.

#### Problème de driver - solution 1

Si vous lisez cette partie vous avez un problème lié à JOGL, qui ne fonctionne pas sur votre machine en raison d'un problème d'index à -1 (exception de type ``IndexOutOfBoundsException``). Il faut ajouter les arguments suivant à la JVM: <br/>
```--add-exports java.base/java.lang=ALL-UNNAMED  --add-exports java.desktop/sun.awt=ALL-UNNAMED --add-exports java.desktop/sun.java2d=ALL-UNNAMED```

Pour cela cliquer sur la bordure gauche sur la partie ``Run & Debug`` (voir image ci-dessous) et ensuite cliquez pour la création d'un fichier de configuration pour le Java.

![](images/jerboatuto_vscode_creatlauncher.png)

Vous devriez voir un fichier JSON apparaitre avec les classes contenant la fonction ``main`` dans le projet. Il suffit alors de chercher celle qui correspond à notre classe ``jerboa.tuto.launcher.JTLauncher``
et d'ajouter la ligne suivant qui va ajouter des arguments à la JVM:
```
"vmArgs": "--add-exports java.base/java.lang=ALL-UNNAMED  --add-exports java.desktop/sun.awt=ALL-UNNAMED --add-exports java.desktop/sun.java2d=ALL-UNNAMED"
```

Votre fichier devrait ressembler à quelque chose comme ça:

![](images/jerboatuto_vscode_configJVM.png)

Vous pouvez relancer la classe ``jerboa.tuto.launcher.JTLauncher`` et normalement l'application devrait se lancer au bout de quelques secondes.

#### Problème de driver - solution 2

Pour une machine sous Linux sans carte graphique dédiée avec chipset Intel (i.e., Intel(R) HD Graphics), il est possible que la carte ne supporte pas OpenGL Core.

L'erreur rencontrée correspond alors à ``Profile GL4bc is not available on X11GraphicsDevice``. Il faut alors ajouter ou compléter l'entrée `vmArgs` dans le fichier laucher pour désactiver OpenGL Core dans la JVM:

```
"vmArgs": "-Djogl.disable.openglcore=false"
```

-----------------------------------------------------------------------------
[Retour à la page d'accueil](README.md)