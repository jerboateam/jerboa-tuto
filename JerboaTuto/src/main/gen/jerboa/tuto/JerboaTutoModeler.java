package jerboa.tuto;

import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import jerboa.tuto.duplication.Duplicate;
import jerboa.tuto.creat.CreatSquare;
import jerboa.tuto.sew.SewA0;
import jerboa.tuto.sew.SewA1;
import jerboa.tuto.sew.SewA2;
import jerboa.tuto.sew.SewA3;
import jerboa.tuto.sew.UnSewA0;
import jerboa.tuto.sew.UnSewA1;
import jerboa.tuto.sew.UnSewA2;
import jerboa.tuto.sew.UnSewA3;
import jerboa.tuto.subdivision.DooSabin3D;
import jerboa.tuto.subdivision.Menger3D;
import jerboa.tuto.subdivision.SubdivideLoop;
import jerboa.tuto.subdivision.SubdivideCatmullClark;
import jerboa.tuto.suppression.DeleteConnex;
import jerboa.tuto.suppression.DeleteFace;
import jerboa.tuto.translation.TranslateVertex;
import jerboa.tuto.translation.TranslateFace;
import jerboa.tuto.translation.TranslateConnex;
import jerboa.tuto.translation.TranslateVolume;
import jerboa.tuto.triangulation.TriangulationAllFaces;
import jerboa.tuto.triangulation.TriangulationFace;
import jerboa.tuto.rotation.RotationConnex;
import jerboa.tuto.util.FlipOrient;
import jerboa.tuto.util.RegenOrient;
import jerboa.tuto.util.SetOrient;
import jerboa.tuto.solution.SolExtrudeFace;
import jerboa.tuto.solution.SolLinkEdges;
import jerboa.tuto.solution.SolTriangulateScript;
import jerboa.tuto.solution.SolExtrudeAlongPath;
import jerboa.tuto.solution.SolExtrudeDir;
import jerboa.tuto.solution.SolExtrudeDirUI;
import jerboa.tuto.extrude.ExtrudeA3;
import jerboa.tuto.solution.SolExtrudeAlongPathUI;
import jerboa.tuto.solution.SolExtrudeSurface;
import jerboa.tuto.util.FlipNormal;
import jerboa.tuto.translation.TranslateFaceUI;
import jerboa.tuto.subdivision.SubdivideCatmullClark3D;
import jerboa.tuto.creat.CreatDart;
import jerboa.tuto.subdivision.SubdivideLoop3D;



import jerboa.tuto.util.*;

/**
 * Demonstration modeler for the Jerboa Tutoriel
 */

public class JerboaTutoModeler extends JerboaModelerGeneric {

    // BEGIN LIST OF EMBEDDINGS
    protected JerboaEmbeddingInfo pos;
    protected JerboaEmbeddingInfo normal;
    protected JerboaEmbeddingInfo color;
    protected JerboaEmbeddingInfo orient;
    // END LIST OF EMBEDDINGS

    // BEGIN USER DECLARATION
    // END USER DECLARATION

    public JerboaTutoModeler() throws JerboaException {

        super(3);

    // BEGIN USER HEAD CONSTRUCTOR TRANSLATION

    // END USER HEAD CONSTRUCTOR TRANSLATION
        pos = new JerboaEmbeddingInfo("pos", JerboaOrbit.orbit(1,2,3), jerboa.tuto.ebds.Vec3.class);
        normal = new JerboaEmbeddingInfo("normal", JerboaOrbit.orbit(0,1), jerboa.tuto.ebds.Vec3.class);
        color = new JerboaEmbeddingInfo("color", JerboaOrbit.orbit(0,1), jerboa.tuto.ebds.Color4.class);
        orient = new JerboaEmbeddingInfo("orient", JerboaOrbit.orbit(), java.lang.Boolean.class);

        this.registerEbdsAndResetGMAP(pos,normal,color,orient);

        this.registerRule(new Duplicate(this));
        this.registerRule(new CreatSquare(this));
        this.registerRule(new SewA0(this));
        this.registerRule(new SewA1(this));
        this.registerRule(new SewA2(this));
        this.registerRule(new SewA3(this));
        this.registerRule(new UnSewA0(this));
        this.registerRule(new UnSewA1(this));
        this.registerRule(new UnSewA2(this));
        this.registerRule(new UnSewA3(this));
        this.registerRule(new DooSabin3D(this));
        this.registerRule(new Menger3D(this));
        this.registerRule(new SubdivideLoop(this));
        this.registerRule(new SubdivideCatmullClark(this));
        this.registerRule(new DeleteConnex(this));
        this.registerRule(new DeleteFace(this));
        this.registerRule(new TranslateVertex(this));
        this.registerRule(new TranslateFace(this));
        this.registerRule(new TranslateConnex(this));
        this.registerRule(new TranslateVolume(this));
        this.registerRule(new TriangulationAllFaces(this));
        this.registerRule(new TriangulationFace(this));
        this.registerRule(new RotationConnex(this));
        this.registerRule(new FlipOrient(this));
        this.registerRule(new RegenOrient(this));
        this.registerRule(new SetOrient(this));
        this.registerRule(new SolExtrudeFace(this));
        this.registerRule(new SolLinkEdges(this));
        this.registerRule(new SolTriangulateScript(this));
        this.registerRule(new SolExtrudeAlongPath(this));
        this.registerRule(new SolExtrudeDir(this));
        this.registerRule(new SolExtrudeDirUI(this));
        this.registerRule(new ExtrudeA3(this));
        this.registerRule(new SolExtrudeAlongPathUI(this));
        this.registerRule(new SolExtrudeSurface(this));
        this.registerRule(new FlipNormal(this));
        this.registerRule(new TranslateFaceUI(this));
        this.registerRule(new SubdivideCatmullClark3D(this));
        this.registerRule(new CreatDart(this));
        this.registerRule(new SubdivideLoop3D(this));
    }

    public final JerboaEmbeddingInfo getPos() {
        return pos;
    }

    public final JerboaEmbeddingInfo getNormal() {
        return normal;
    }

    public final JerboaEmbeddingInfo getColor() {
        return color;
    }

    public final JerboaEmbeddingInfo getOrient() {
        return orient;
    }

}
