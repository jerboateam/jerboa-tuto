package jerboa.tuto.rotation;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Color4;
import java.lang.Boolean;
 // BEGIN HEADER IMPORT

import javax.swing.JOptionPane;

 // END HEADER IMPORT



/**
 * 
 */



public class RotationConnex extends JerboaRuleGenerated {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 

	protected double angle; 
	protected Vec3 vect; 

	// END PARAMETERS 

 // BEGIN COPY PASTE OF HEADER



 // END COPY PASTE OF HEADER


    public RotationConnex(JerboaTutoModeler modeler) throws JerboaException {

        super(modeler, "RotationConnex", "rotation");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(0,1,2,3), 3);
        left.add(ln0);
        hooks.add(ln0);

        // -------- RIGHT GRAPH
        JerboaRuleNode rn0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(0,1,2,3), 3, new RotationConnexExprRn0pos());
        right.add(rn0);
;
        // ------- SPECIFIED FEATURE
        computeEfficientTopoStructure();
        computeSpreadOperation();
        // ------- USER DECLARATION 
 // BEGIN COPY PASTE OF HEADER

 // END COPY PASTE OF HEADER
        angle = 1.5707963267948966192313216916398;        vect = new Vec3(0,1,0);    }

    public int reverseAssoc(int i) {
        switch(i) {
        case 0: return 0;
        }
        return -1;
    }

    public int attachedNode(int i) {
        switch(i) {
        case 0: return 0;
        }
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0, double angle, Vec3 vect) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        setAngle(angle);
        setVect(vect);
        return applyRule(gmap, ____jme_hooks);
	}

    private class RotationConnexExprRn0pos implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Vec3.rotation(curleftPattern.getNode(0).<jerboa.tuto.ebds.Vec3>ebd(0),vect,angle);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "pos";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getPos().getID();
        }
    }

    @Override
    public boolean hasMidprocess() { return true; }
    @Override
    public boolean midprocess(JerboaGMap gmap, List<JerboaRowPattern> leftPattern) throws JerboaException {
	// BEGIN MIDPROCESS CODE
vect = Vec3.askVec3("Rotation axis: ",vect);
String p = JOptionPane.showInputDialog("Angle (degre): ",Math.toDegrees(angle));
double rot = Double.parseDouble(p);
angle = Math.toRadians(rot);
return true;

	// END MIDPROCESS CODE
    }

    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

	public double getAngle(){
		return angle;
	}
	public void setAngle(double _angle){
		this.angle = _angle;
	}
	public Vec3 getVect(){
		return vect;
	}
	public void setVect(Vec3 _vect){
		this.vect = _vect;
	}
} // end rule Class