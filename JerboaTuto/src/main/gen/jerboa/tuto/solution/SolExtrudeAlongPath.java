package jerboa.tuto.solution;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Color4;
import java.lang.Boolean;
 // BEGIN HEADER IMPORT

 // END HEADER IMPORT

/* Raw Imports : */

import jerboa.tuto.util.Vec3List;
import jerboa.tuto.solution.SolExtrudeDir;
import jerboa.tuto.extrude.ExtrudeA3;

/* End raw Imports */



/**
 * 
 */



public class SolExtrudeAlongPath extends JerboaRuleScript {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 

	protected Vec3List path; 

	// END PARAMETERS 

 // BEGIN COPY PASTE OF HEADER



 // END COPY PASTE OF HEADER


    public SolExtrudeAlongPath(JerboaTutoModeler modeler) throws JerboaException {

        super(modeler, "SolExtrudeAlongPath", "solution");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(), 3);
        left.add(ln0);
        hooks.add(ln0);

        // -------- RIGHT GRAPH
        JerboaRuleNode rn0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(), 3);
        right.add(rn0);
;
        // ------- USER DECLARATION 
 // BEGIN COPY PASTE OF HEADER

 // END COPY PASTE OF HEADER
    }

    public int reverseAssoc(int i) {
        switch(i) {
        case 0: return 0;
        }
        return -1;
    }

    public int attachedNode(int i) {
        switch(i) {
        case 0: return 0;
        }
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0, Vec3List path) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        setPath(path);
        return applyRule(gmap, ____jme_hooks);
	}

@Override
    public JerboaRuleResult apply(final JerboaGMap gmap, final JerboaInputHooks hooks) throws JerboaException {
// BEGIN SCRIPT GENERATION
        for(JerboaDart dart : hooks){
		   if((path.size() > 0)) {
		      JerboaInputHooksGeneric _v_hook3 = new JerboaInputHooksGeneric();
		      _v_hook3.addCol(hooks.dart(0,0));
		      ((SolExtrudeDir)modeler.getRule("SolExtrudeDir")).setDir(path.get(0));
		      JerboaRuleResult resExDir = ((SolExtrudeDir)modeler.getRule("SolExtrudeDir")).applyRule(gmap, _v_hook3);
		      for(int i=1;i<=(path.size() - 1);i+=1){
		         JerboaInputHooksGeneric _v_hook4 = new JerboaInputHooksGeneric();
		         _v_hook4.addCol(resExDir.get("n5",0));
		         JerboaRuleResult resExA3 = ((ExtrudeA3)modeler.getRule("ExtrudeA3")).applyRule(gmap, _v_hook4);
		         JerboaInputHooksGeneric _v_hook5 = new JerboaInputHooksGeneric();
		         _v_hook5.addCol(resExA3.get("n1",0));
		         ((SolExtrudeDir)modeler.getRule("SolExtrudeDir")).setDir(path.get(i));
		         resExDir = ((SolExtrudeDir)modeler.getRule("SolExtrudeDir")).applyRule(gmap, _v_hook5);
		      }
		   }
		}
		
		return null;
		// END SCRIPT GENERATION

	}
    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

	public Vec3List getPath(){
		return path;
	}
	public void setPath(Vec3List _path){
		this.path = _path;
	}
} // end rule Class