package jerboa.tuto.solution;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Color4;
import java.lang.Boolean;

/* Raw Imports : */

import jerboa.tuto.util.Vec3List;
import jerboa.tuto.solution.SolExtrudeAlongPath;

/* End raw Imports */



/**
 * 
 */



public class SolExtrudeAlongPathUI extends JerboaRuleScript {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public SolExtrudeAlongPathUI(JerboaTutoModeler modeler) throws JerboaException {

        super(modeler, "SolExtrudeAlongPathUI", "solution");

        // -------- LEFT GRAPH

        // -------- RIGHT GRAPH
;
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        return -1;
    }

    public int attachedNode(int i) {
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        return applyRule(gmap, ____jme_hooks);
	}

@Override
    public JerboaRuleResult apply(final JerboaGMap gmap, final JerboaInputHooks hooks) throws JerboaException {
// BEGIN SCRIPT GENERATION
        Vec3List dirs = new Vec3List();
		int count = Vec3.askInteger("How long is the path?",3);
		for(int i=1;i<=count;i+=1){
		   Vec3 dir = Vec3.askVec3(("Vector " + i),new Vec3(0,1,0));
		   dirs.add(dir);
		}
		JerboaInputHooksGeneric _v_hook0 = new JerboaInputHooksGeneric();
		_v_hook0.addCol(hooks.dart(0));
		((SolExtrudeAlongPath)modeler.getRule("SolExtrudeAlongPath")).setPath(dirs);
		return ((SolExtrudeAlongPath)modeler.getRule("SolExtrudeAlongPath")).applyRule(gmap, _v_hook0);
		// END SCRIPT GENERATION

	}
} // end rule Class