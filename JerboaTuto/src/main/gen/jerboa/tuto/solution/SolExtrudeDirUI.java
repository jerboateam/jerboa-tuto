package jerboa.tuto.solution;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Color4;
import java.lang.Boolean;

/* Raw Imports : */
import jerboa.tuto.solution.SolExtrudeDir;

/* End raw Imports */



/**
 * 
 */



public class SolExtrudeDirUI extends JerboaRuleScript {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public SolExtrudeDirUI(JerboaTutoModeler modeler) throws JerboaException {

        super(modeler, "SolExtrudeDirUI", "solution");

        // -------- LEFT GRAPH

        // -------- RIGHT GRAPH
;
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        return -1;
    }

    public int attachedNode(int i) {
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        return applyRule(gmap, ____jme_hooks);
	}

@Override
    public JerboaRuleResult apply(final JerboaGMap gmap, final JerboaInputHooks hooks) throws JerboaException {
// BEGIN SCRIPT GENERATION
        Vec3 dir = Vec3.askVec3("Saissisez le vecteur d'extrusion:",new Vec3(1,0,0));
		JerboaDart d = hooks.dart(0,0);
		JerboaInputHooksGeneric _v_hook0 = new JerboaInputHooksGeneric();
		_v_hook0.addCol(d);
		((SolExtrudeDir)modeler.getRule("SolExtrudeDir")).setDir(dir);
		return ((SolExtrudeDir)modeler.getRule("SolExtrudeDir")).applyRule(gmap, _v_hook0);
		// END SCRIPT GENERATION

	}
} // end rule Class