package jerboa.tuto.solution;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Color4;
import java.lang.Boolean;

/* Raw Imports : */
import jerboa.tuto.triangulation.TriangulationFace;

/* End raw Imports */



/**
 * 
 */



public class SolTriangulateScript extends JerboaRuleScript {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public SolTriangulateScript(JerboaTutoModeler modeler) throws JerboaException {

        super(modeler, "SolTriangulateScript", "solution");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(), 3);
        left.add(ln0);
        hooks.add(ln0);

        // -------- RIGHT GRAPH
        JerboaRuleNode rn0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(), 3);
        right.add(rn0);
;
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        switch(i) {
        case 0: return 0;
        }
        return -1;
    }

    public int attachedNode(int i) {
        switch(i) {
        case 0: return 0;
        }
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        return applyRule(gmap, ____jme_hooks);
	}

@Override
    public JerboaRuleResult apply(final JerboaGMap gmap, final JerboaInputHooks hooks) throws JerboaException {
// BEGIN SCRIPT GENERATION
        for(JerboaDart dart : gmap){
		   if((dart.alpha(0).alpha(1).alpha(0).alpha(1).alpha(0).alpha(1) != dart)) {
		      JerboaInputHooksGeneric _v_hook1 = new JerboaInputHooksGeneric();
		      _v_hook1.addCol(dart);
		      ((TriangulationFace)modeler.getRule("TriangulationFace")).applyRule(gmap, _v_hook1);
		   }
		}
		
		return null;
		// END SCRIPT GENERATION

	}
    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

} // end rule Class