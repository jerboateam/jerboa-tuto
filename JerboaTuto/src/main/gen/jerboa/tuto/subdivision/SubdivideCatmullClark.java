package jerboa.tuto.subdivision;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Color4;
import java.lang.Boolean;



/**
 * 
 */



public class SubdivideCatmullClark extends JerboaRuleGenerated {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public SubdivideCatmullClark(JerboaTutoModeler modeler) throws JerboaException {

        super(modeler, "SubdivideCatmullClark", "subdivision");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(0,1,2), 3);
        left.add(ln0);
        hooks.add(ln0);
        ln0.setAlpha(3, ln0);

        // -------- RIGHT GRAPH
        JerboaRuleNode rn0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(-1,1,2), 3, new SubdivideCatmullClarkExprRn0pos());
        JerboaRuleNode rn1 = new JerboaRuleNode("n1", 1, JerboaOrbit.orbit(-1,-1,2), 3, new SubdivideCatmullClarkExprRn1pos(), new SubdivideCatmullClarkExprRn1orient());
        JerboaRuleNode rn2 = new JerboaRuleNode("n2", 2, JerboaOrbit.orbit(2,-1,-1), 3, new SubdivideCatmullClarkExprRn2orient());
        JerboaRuleNode rn3 = new JerboaRuleNode("n3", 3, JerboaOrbit.orbit(2,1,-1), 3, new SubdivideCatmullClarkExprRn3orient(), new SubdivideCatmullClarkExprRn3pos());
        right.add(rn0);
        right.add(rn1);
        right.add(rn2);
        right.add(rn3);
        rn0.setAlpha(0, rn1);
        rn1.setAlpha(1, rn2);
        rn2.setAlpha(0, rn3);
        rn0.setAlpha(3, rn0);
        rn1.setAlpha(3, rn1);
        rn2.setAlpha(3, rn2);
        rn3.setAlpha(3, rn3);
;
        // ------- SPECIFIED FEATURE
        computeEfficientTopoStructure();
        computeSpreadOperation();
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return -1;
        case 2: return -1;
        case 3: return -1;
        }
        return -1;
    }

    public int attachedNode(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return 0;
        case 2: return 0;
        case 3: return 0;
        }
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        return applyRule(gmap, ____jme_hooks);
	}

    private class SubdivideCatmullClarkExprRn0pos implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Vec3 old_coord = new Vec3(curleftPattern.getNode(0).<jerboa.tuto.ebds.Vec3>ebd(0));
int n_faces = 0;
Vec3 avg_face_poss = new Vec3();
for(JerboaDart dart : gmap.collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),JerboaOrbit.orbit(0,1))){
   Vec3 mid = Vec3.middle(gmap.collect(dart,JerboaOrbit.orbit(0,1),JerboaOrbit.orbit(0)),"pos");
   avg_face_poss.add(mid);
   n_faces ++ ;
}

avg_face_poss.scale((1.0 / n_faces));
Vec3 avg_edge_poss = new Vec3();
int n_edges = 0;
for(JerboaDart dart : gmap.collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),JerboaOrbit.orbit(2,3))){
   Vec3 edgeMid = Vec3.middle(gmap.<jerboa.tuto.ebds.Vec3>collect(dart,JerboaOrbit.orbit(0),0));
   Vec3 face1Mid = Vec3.middle(gmap.<jerboa.tuto.ebds.Vec3>collect(dart,JerboaOrbit.orbit(0,1),0));
   Vec3 face2Mid = Vec3.middle(gmap.<jerboa.tuto.ebds.Vec3>collect(dart.alpha(2),JerboaOrbit.orbit(0,1),0));
   edgeMid.add(face1Mid);
   edgeMid.add(face2Mid);
   edgeMid.scale((1.0 / 3.0));
   avg_edge_poss.add(edgeMid);
   n_edges ++ ;
}

avg_edge_poss.scale((1.0 / n_edges));
double m1 = ((n_faces - 3.0) / n_faces);
double m2 = (1.0 / n_faces);
double m3 = (2.0 / n_faces);
old_coord.scale(m1);
avg_face_poss.scale(m2);
avg_edge_poss.scale(m3);
old_coord.add(avg_face_poss);
old_coord.add(avg_edge_poss);
return old_coord;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "pos";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getPos().getID();
        }
    }

    private class SubdivideCatmullClarkExprRn1pos implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Vec3 edgeMid = Vec3.middle(curleftPattern.getNode(0).<jerboa.tuto.ebds.Vec3>ebd(0),curleftPattern.getNode(0).alpha(0).<jerboa.tuto.ebds.Vec3>ebd(0));
Vec3 face1Mid = Vec3.middle(gmap.<jerboa.tuto.ebds.Vec3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1),0));
Vec3 face2Mid = Vec3.middle(gmap.<jerboa.tuto.ebds.Vec3>collect(curleftPattern.getNode(0).alpha(2),JerboaOrbit.orbit(0,1),0));
edgeMid.add(face1Mid);
edgeMid.add(face2Mid);
edgeMid.scale((1.0 / 3.0));
return edgeMid;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "pos";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getPos().getID();
        }
    }

    private class SubdivideCatmullClarkExprRn1orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return !(curleftPattern.getNode(0).<java.lang.Boolean>ebd(3));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getOrient().getID();
        }
    }

    private class SubdivideCatmullClarkExprRn2orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return curleftPattern.getNode(0).<java.lang.Boolean>ebd(3);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getOrient().getID();
        }
    }

    private class SubdivideCatmullClarkExprRn3orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return !(curleftPattern.getNode(0).<java.lang.Boolean>ebd(3));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getOrient().getID();
        }
    }

    private class SubdivideCatmullClarkExprRn3pos implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Vec3.middle(gmap.<jerboa.tuto.ebds.Vec3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1),0));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "pos";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getPos().getID();
        }
    }

    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

} // end rule Class