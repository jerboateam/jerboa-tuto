package jerboa.tuto.subdivision;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Color4;
import java.lang.Boolean;



/**
 * 
 */



public class SubdivideLoop3D extends JerboaRuleGenerated {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public SubdivideLoop3D(JerboaTutoModeler modeler) throws JerboaException {

        super(modeler, "SubdivideLoop3D", "subdivision");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(0,1,2,3), 3);
        left.add(ln0);
        hooks.add(ln0);

        // -------- RIGHT GRAPH
        JerboaRuleNode rn0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(-1,1,2,3), 3, new SubdivideLoop3DExprRn0pos());
        JerboaRuleNode rn1 = new JerboaRuleNode("n1", 1, JerboaOrbit.orbit(-1,-1,2,3), 3, new SubdivideLoop3DExprRn1pos(), new SubdivideLoop3DExprRn1orient());
        JerboaRuleNode rn2 = new JerboaRuleNode("n2", 2, JerboaOrbit.orbit(-1,0,-1,3), 3, new SubdivideLoop3DExprRn2orient());
        JerboaRuleNode rn3 = new JerboaRuleNode("n3", 3, JerboaOrbit.orbit(1,0,-1,3), 3, new SubdivideLoop3DExprRn3color(), new SubdivideLoop3DExprRn3orient(), new SubdivideLoop3DExprRn3normal());
        right.add(rn0);
        right.add(rn1);
        right.add(rn2);
        right.add(rn3);
        rn0.setAlpha(0, rn1);
        rn1.setAlpha(1, rn2);
        rn2.setAlpha(2, rn3);
;
        // ------- SPECIFIED FEATURE
        computeEfficientTopoStructure();
        computeSpreadOperation();
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return -1;
        case 2: return -1;
        case 3: return -1;
        }
        return -1;
    }

    public int attachedNode(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return 0;
        case 2: return 0;
        case 3: return 0;
        }
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        return applyRule(gmap, ____jme_hooks);
	}

    private class SubdivideLoop3DExprRn0pos implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
java.util.List<Vec3> voisins = new ArrayList<Vec3>();
for(JerboaDart d : gmap.collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2),JerboaOrbit.orbit(2))){
   voisins.add(d.alpha(0).<jerboa.tuto.ebds.Vec3>ebd(0));
}

int n = voisins.size();
Vec3 mid = Vec3.middle(voisins);
if((n <= 1)) {
   return curleftPattern.getNode(0).<jerboa.tuto.ebds.Vec3>ebd(0);
}
else {
   if((n == 2)) {
      return Vec3.barycenter(curleftPattern.getNode(0).<jerboa.tuto.ebds.Vec3>ebd(0),(3.0 / 4.0),mid,(1.0 / 4.0));
   }
   else {
      if((n == 3)) {
         return Vec3.barycenter(curleftPattern.getNode(0).<jerboa.tuto.ebds.Vec3>ebd(0),(7.0 / 16.0),mid,(9.0 / 16.0));
      }
      else {
         double beta = (3.0 / 8.0);
         double opp = (1 - beta);
         return Vec3.barycenter(curleftPattern.getNode(0).<jerboa.tuto.ebds.Vec3>ebd(0),opp,mid,beta);
      }
   }
}
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "pos";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getPos().getID();
        }
    }

    private class SubdivideLoop3DExprRn1pos implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Vec3.barycenter(curleftPattern.getNode(0).<jerboa.tuto.ebds.Vec3>ebd(0),(3.0 / 8.0),curleftPattern.getNode(0).alpha(0).<jerboa.tuto.ebds.Vec3>ebd(0),(3.0 / 8.0),curleftPattern.getNode(0).alpha(1).alpha(0).<jerboa.tuto.ebds.Vec3>ebd(0),(1.0 / 8.0),curleftPattern.getNode(0).alpha(2).alpha(1).alpha(0).<jerboa.tuto.ebds.Vec3>ebd(0),(1.0 / 8.0));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "pos";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getPos().getID();
        }
    }

    private class SubdivideLoop3DExprRn1orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return !(curleftPattern.getNode(0).<java.lang.Boolean>ebd(3));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getOrient().getID();
        }
    }

    private class SubdivideLoop3DExprRn2orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return curleftPattern.getNode(0).<java.lang.Boolean>ebd(3);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getOrient().getID();
        }
    }

    private class SubdivideLoop3DExprRn3color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color4.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getColor().getID();
        }
    }

    private class SubdivideLoop3DExprRn3orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return !(curleftPattern.getNode(0).<java.lang.Boolean>ebd(3));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getOrient().getID();
        }
    }

    private class SubdivideLoop3DExprRn3normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return curleftPattern.getNode(0).<jerboa.tuto.ebds.Vec3>ebd(1);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getNormal().getID();
        }
    }

    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

} // end rule Class