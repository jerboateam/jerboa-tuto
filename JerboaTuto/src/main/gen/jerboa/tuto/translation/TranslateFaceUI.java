package jerboa.tuto.translation;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Color4;
import java.lang.Boolean;

/* Raw Imports : */
import jerboa.tuto.translation.TranslateFace;

/* End raw Imports */



/**
 * 
 */



public class TranslateFaceUI extends JerboaRuleScript {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 

	protected Vec3 vect; 

	// END PARAMETERS 



    public TranslateFaceUI(JerboaTutoModeler modeler) throws JerboaException {

        super(modeler, "TranslateFaceUI", "translation");

        // -------- LEFT GRAPH

        // -------- RIGHT GRAPH
;
        // ------- USER DECLARATION 
        vect = new Vec3(0,1,0);;    }

    public int reverseAssoc(int i) {
        return -1;
    }

    public int attachedNode(int i) {
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, Vec3 vect) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        setVect(vect);
        return applyRule(gmap, ____jme_hooks);
	}

@Override
    public JerboaRuleResult apply(final JerboaGMap gmap, final JerboaInputHooks hooks) throws JerboaException {
// BEGIN SCRIPT GENERATION
        vect = Vec3.askVec3("Input of translation vector:",vect);
		for(JerboaDart dart : hooks){
		   JerboaInputHooksGeneric _v_hook0 = new JerboaInputHooksGeneric();
		   _v_hook0.addCol(dart);
		   ((TranslateFace)modeler.getRule("TranslateFace")).setVect(vect);
		   ((TranslateFace)modeler.getRule("TranslateFace")).applyRule(gmap, _v_hook0);
		}
		
		return null;
		// END SCRIPT GENERATION

	}
	public Vec3 getVect(){
		return vect;
	}
	public void setVect(Vec3 _vect){
		this.vect = _vect;
	}
} // end rule Class