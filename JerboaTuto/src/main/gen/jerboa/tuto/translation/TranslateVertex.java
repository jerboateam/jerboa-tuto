package jerboa.tuto.translation;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Color4;
import java.lang.Boolean;



/**
 * 
 */



public class TranslateVertex extends JerboaRuleGenerated {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 

	protected Vec3 vect; 

	// END PARAMETERS 



    public TranslateVertex(JerboaTutoModeler modeler) throws JerboaException {

        super(modeler, "TranslateVertex", "translation");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(1,2,3), 3);
        left.add(ln0);
        hooks.add(ln0);

        // -------- RIGHT GRAPH
        JerboaRuleNode rn0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(1,2,3), 3, new TranslateVertexExprRn0pos());
        right.add(rn0);
;
        // ------- SPECIFIED FEATURE
        computeEfficientTopoStructure();
        computeSpreadOperation();
        // ------- USER DECLARATION 
        vect = new Vec3(1,0,0);    }

    public int reverseAssoc(int i) {
        switch(i) {
        case 0: return 0;
        }
        return -1;
    }

    public int attachedNode(int i) {
        switch(i) {
        case 0: return 0;
        }
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0, Vec3 vect) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        setVect(vect);
        return applyRule(gmap, ____jme_hooks);
	}

    private class TranslateVertexExprRn0pos implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Vec3 p = new Vec3(curleftPattern.getNode(0).<jerboa.tuto.ebds.Vec3>ebd(0));
return p.add(vect);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "pos";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaTutoModeler)modeler).getPos().getID();
        }
    }

    @Override
    public boolean hasMidprocess() { return true; }
    @Override
    public boolean midprocess(JerboaGMap gmap, List<JerboaRowPattern> leftPattern) throws JerboaException {
	// BEGIN MIDPROCESS CODE
vect = Vec3.askVec3("Translation vector: ",vect);
return (vect != null);

	// END MIDPROCESS CODE
    }

    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

	public Vec3 getVect(){
		return vect;
	}
	public void setVect(Vec3 _vect){
		this.vect = _vect;
	}
} // end rule Class