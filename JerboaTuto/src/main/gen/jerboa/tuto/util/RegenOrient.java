package jerboa.tuto.util;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.ebds.Color4;
import java.lang.Boolean;
 // BEGIN HEADER IMPORT

import java.util.Stack;

 // END HEADER IMPORT

/* Raw Imports : */
import jerboa.tuto.util.SetOrient;

/* End raw Imports */



/**
 * 
 */



public class RegenOrient extends JerboaRuleScript {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 

 // BEGIN COPY PASTE OF HEADER



 // END COPY PASTE OF HEADER


    public RegenOrient(JerboaTutoModeler modeler) throws JerboaException {

        super(modeler, "RegenOrient", "util");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(), 3);
        left.add(ln0);
        hooks.add(ln0);

        // -------- RIGHT GRAPH
        JerboaRuleNode rn0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(), 3);
        right.add(rn0);
;
        // ------- USER DECLARATION 
 // BEGIN COPY PASTE OF HEADER

 // END COPY PASTE OF HEADER
    }

    public int reverseAssoc(int i) {
        switch(i) {
        case 0: return 0;
        }
        return -1;
    }

    public int attachedNode(int i) {
        switch(i) {
        case 0: return 0;
        }
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        return applyRule(gmap, ____jme_hooks);
	}

@Override
    public JerboaRuleResult apply(final JerboaGMap gmap, final JerboaInputHooks hooks) throws JerboaException {
// BEGIN SCRIPT GENERATION
        Stack<JerboaDart> darts = new Stack<JerboaDart>();
		boolean flag = true;
		for(JerboaDart d : hooks){
		   darts.push(d);
		   JerboaInputHooksGeneric _v_hook0 = new JerboaInputHooksGeneric();
		   _v_hook0.addCol(d);
		   ((SetOrient)modeler.getRule("SetOrient")).setFlag(flag);
		   ((SetOrient)modeler.getRule("SetOrient")).applyRule(gmap, _v_hook0);
		}
		
		JerboaMark marker = gmap.creatFreeMarker();
		while(!(darts.isEmpty())){
		   JerboaDart d = darts.pop();
		   if(d.isNotMarked(marker)) {
		      gmap.mark(marker, d);
		      for(int i=0;i<=modeler.getDimension();i+=1){
		         if(!(d.isFree(i))) {
		            JerboaInputHooksGeneric _v_hook4 = new JerboaInputHooksGeneric();
		            _v_hook4.addCol(d.alpha(i));
		            ((SetOrient)modeler.getRule("SetOrient")).setFlag(!(d.<java.lang.Boolean>ebd(3)));
		            ((SetOrient)modeler.getRule("SetOrient")).applyRule(gmap, _v_hook4);
		            darts.push(d.alpha(i));
		         }
		      }
		   }
		}
		
		gmap.freeMarker(marker);
		return null;
		// END SCRIPT GENERATION

	}
    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

} // end rule Class