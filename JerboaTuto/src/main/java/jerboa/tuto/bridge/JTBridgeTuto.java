package jerboa.tuto.bridge;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.up.xlim.sic.ig.jerboa.trigger.tools.JerboaMonitorInfo;
import fr.up.xlim.sic.ig.jerboa.trigger.tools.JerboaMonitorInfoConsole;
import fr.up.xlim.sic.ig.jerboa.viewer.IJerboaModelerViewer;
import fr.up.xlim.sic.ig.jerboa.viewer.tools.GMapViewerBridge;
import fr.up.xlim.sic.ig.jerboa.viewer.tools.GMapViewerColor;
import fr.up.xlim.sic.ig.jerboa.viewer.tools.GMapViewerPoint;
import fr.up.xlim.sic.ig.jerboa.viewer.tools.GMapViewerTuple;
import fr.up.xlim.sic.ig.jerboa.viewer.tools.JerboaMonitorInfoBridgeSerializerMonitor;
import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.bridge.command.JTHelloWorld;
import jerboa.tuto.bridge.command.JTCommand;
import jerboa.tuto.ebds.Color4;
import jerboa.tuto.ebds.Vec3;
import jerboa.tuto.io.jba.JBASerializer;
import jerboa.tuto.io.obj.OBJLoader;
import jerboa.tuto.io.stl.STLLoader;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaEmbeddingInfo;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaGMapArray;
import up.jerboa.core.JerboaModeler;
import up.jerboa.core.util.JerboaGMapDuplicateFactory;
import up.jerboa.core.util.Pair;
import up.jerboa.exception.JerboaException;
import up.jerboa.exception.JerboaGMapDuplicateException;
import up.jerboa.util.ConsoleJerboaSerializerMonitor;
import up.jerboa.util.serialization.JerboaSerializeException;
import up.jerboa.util.serialization.jba.JBAFormat;

public class JTBridgeTuto implements GMapViewerBridge, JerboaGMapDuplicateFactory {
	private JerboaTutoModeler modeler;
	private List<JTCommand> commands;
	private int ebdPosID;
	private int ebdNormID;
	private int ebdColorID;
	private int ebdOrientID;
	
	private transient JerboaMonitorInfo info; // DO NOT USE IT
	
	public JTBridgeTuto(JerboaTutoModeler modeler) {
		this.modeler = modeler;
		this.commands = new ArrayList<>();
		
		this.ebdColorID = modeler.getColor().getID();
		this.ebdPosID = modeler.getPos().getID();
		this.ebdNormID = modeler.getNormal().getID();
		this.ebdOrientID = modeler.getOrient().getID();
		
		
		initializeCommands();
	}
	
	private void initializeCommands() {
		commands.add(new JTHelloWorld());
	}

	@Override
	public Object duplicate(JerboaEmbeddingInfo info, Object value) {
		return value;
	}

	@Override
	public JerboaEmbeddingInfo convert(JerboaEmbeddingInfo info) {
		return info;
	}

	@Override
	public boolean manageEmbedding(JerboaEmbeddingInfo info) {
		switch(info.getName()) {
		case "pos": return true;
		case "normal": return true;
		case "color":return true;
		case "orient":return true;
		}
		return true;
	}

	@Override
	public boolean canUndo() {
		return true;
	}

	@Override
	public GMapViewerColor colors(JerboaDart dart) {
		Color4 color = dart.<Color4>ebd(ebdColorID);
		
		if(color == null) {
			System.err.println("No color for dart " + dart.getID());
			color = Color4.LIGHTGRAY;
		}
		
		GMapViewerColor col = new GMapViewerColor(color.getR(), color.getG(), color.getB(), color.getA());
		return col;
	}

	@Override
	public GMapViewerPoint coords(JerboaDart dart) {
		try {
			Vec3 p = dart.<Vec3> ebd(ebdPosID);
			GMapViewerPoint res = new GMapViewerPoint((float) p.getX(), (float) p.getY(), (float) p.getZ());
			return res;
		} catch (NullPointerException e) {
			System.err.println("No position for dart " + dart.getID());
			throw e;
		}
	}
	
	@Override
	public GMapViewerTuple normals(JerboaDart dart) {
		Vec3 p = dart.<Vec3> ebd(ebdNormID);
		if(p == null) {
			p = new Vec3(); // on affect le vecteur nul
		}
		GMapViewerPoint res = new GMapViewerPoint((float) p.getX(), (float) p.getY(), (float) p.getZ());
		return res;
	}


	@Override
	public JerboaGMap duplicate(JerboaGMap gmap) throws JerboaGMapDuplicateException {
		JerboaGMap res = new JerboaGMapArray(modeler,gmap.getCapacity());
		gmap.duplicateInGMap(res, this);
		return res;
	}

	@Override
	public List<Pair<String, String>> getCommandLineHelper() {
		
		List<Pair<String, String>> list = commands.parallelStream().map(
					cmd -> { return new Pair<String,String>(cmd.getName(), cmd.getDescription());}
				).collect(Collectors.toList());
		return list;
	}

	@Override
	public JerboaGMap getGMap() {
		return modeler.getGMap();
	}

	@Override
	public JerboaModeler getModeler() {
		return modeler;
	}

	@Override
	public boolean getOrient(JerboaDart dart) {
		return dart.<Boolean>ebd(ebdOrientID);
	}

	@Override
	public boolean hasColor() {
		return true;
	}

	@Override
	public boolean hasNormal() {
		return true;
	}

	@Override
	public boolean hasOrient() {
		return true;
	}
	

	
	@Override
	public boolean parseCommandLine(PrintStream ps, String line) {
		
		StringTokenizer tokenizer = new StringTokenizer(line);
		if(!tokenizer.hasMoreTokens())
			return true;
		
		String cmd = tokenizer.nextToken();
		return commands.parallelStream().filter(c -> c.accept(cmd)).anyMatch(c -> {
			return c.process(line);
		});
	}

	@Override
	public void load(IJerboaModelerViewer viewer, JerboaMonitorInfo info) {
		this.info = info;
		try {
			JFileChooser filec = new JFileChooser("./assets");
			
			FileFilter jbafilter = new FileNameExtensionFilter("JBA format (*.jba)", "jba");
			filec.addChoosableFileFilter(jbafilter);
			FileFilter objfilter = new FileNameExtensionFilter("OBJ file (*.obj)", "obj");
			filec.addChoosableFileFilter(objfilter);
			FileFilter stlfilter = new FileNameExtensionFilter("STL file (*.stl)", "stl");
			filec.addChoosableFileFilter(stlfilter);
			FileFilter allsupported = new FileNameExtensionFilter("All supported (*.stl; *.obj; *.jba)", "stl","obj","jba");
			filec.addChoosableFileFilter(allsupported);
			
			filec.setFileFilter(allsupported);
			if (filec.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				File file = filec.getSelectedFile();
				loadFile(file.getAbsolutePath());
			}
		}
		finally {
			this.info = null;
		}
	}

	@Override
	public void loadFile(String filename) {
		File file = new File(filename);
		if(file.exists()) {
			try(FileInputStream fis = new FileInputStream(file);) {
				
				if(filename.endsWith(".obj")) {
					OBJLoader objformat = new OBJLoader(modeler);
					objformat.parse(fis);
				}
				else if(filename.endsWith(".stl")) {
					STLLoader stlformat = new STLLoader(modeler);
					stlformat.load(file, info == null? new JerboaMonitorInfoConsole() : info);					
				}
				else if(filename.endsWith(".jba")) {
					JBAFormat jbaformat = new JBAFormat(modeler, 
							info != null? new  JerboaMonitorInfoBridgeSerializerMonitor(info) : new ConsoleJerboaSerializerMonitor(), 
							new JBASerializer(modeler));
					
					jbaformat.load(fis);
				}
			}
			catch(Exception ex) {
				System.err.println("Error during loading: " + filename);
				ex.printStackTrace(System.err);
			}
			
		}
	}


	@Override
	public void save(IJerboaModelerViewer arg0, JerboaMonitorInfo arg1) {
		// TODO Auto-generated method stub
		
	}
	
	

}
