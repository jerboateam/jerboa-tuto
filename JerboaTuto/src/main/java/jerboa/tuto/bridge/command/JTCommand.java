package jerboa.tuto.bridge.command;

public interface JTCommand {
	String getName();
	String getDescription();
	
	boolean process(String line);
	
	boolean accept(String ebdname);
}
