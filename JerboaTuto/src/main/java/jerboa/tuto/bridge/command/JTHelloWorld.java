package jerboa.tuto.bridge.command;

public class JTHelloWorld implements JTCommand {

	@Override
	public String getName() {
		return "hello";
	}

	@Override
	public String getDescription() {
		return "Display in console, the word world";
	}

	@Override
	public boolean process(String line) {
		System.out.println("world");
		return true;
	}

	@Override
	public boolean accept(String ebdname) {
		return "hello".equals(ebdname);
	}

}
