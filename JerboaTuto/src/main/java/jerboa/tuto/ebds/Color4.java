package jerboa.tuto.ebds;

import java.awt.Color;
import java.util.List;
import java.util.Random;

import javax.swing.JColorChooser;


/**
 * This class represent an RGBA color in the tutorial modeler of Jerboa.
 * This class is compatible with color in JOGL and java.awt.Color , thus each component is normalized in range [0.0f, 1.f]
 * 
 * 
 * @see java.awt.Color
 * @author Hakim BELHAOUARI
 *
 */

public class Color4 implements Comparable<Color4> {

	public static final double EPSILON = 1e-2;

	private float r, g, b, a;
	
	/** Red color */ 
	public static final Color4 RED = new Color4  (1,0,0);
	
	/** Green color */
	public static final Color4 GREEN = new Color4(0,1,0);
	
	/** Blue color */
	public static final Color4 BLUE = new Color4 (0,0,1);
	
	/** Light gray color */
	public static final Color4 LIGHTGRAY = new Color4 (0.5f,0.5f,0.5f);

	
	/**
	 * Construct a color from R,G,B,A component. Each component must be in range [0.f, 1.f].
	 * 
	 * @param r component of red
	 * @param g component of green
	 * @param b component of blue
	 * @param a component of alpha channel
	 */
	public Color4(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	/**
	 * Construct a color from R,G,B component. Each component must be in range [0.f, 1.f].
	 * The alpha channel is initialized to 1.f.
	 * 
	 * @param r component of red
	 * @param g component of green
	 * @param b component of blue
	 */
	public Color4(float r, float g, float b) {
		this(r, g, b, 1);
	}

	/**
	 * Build a Color4 from the {@link java.awt.Color} present in default JDK.
	 * @param color default representation of Color in Java.
	 */
	public Color4(Color color) {
		float[] rgb = color.getRGBComponents(null);
		r = rgb[0];
		g = rgb[1];
		b = rgb[2];
		a = rgb[3];
	}

	/**
	 * Copy constructor for duplicating object.
	 * @param color already existent Color4.
	 */
	public Color4(Color4 color) {
		r = color.r;
		g = color.g;
		b = color.b;
		a = color.a;
	}

	public float getR() {
		return r;
	}

	public void setR(float r) {
		this.r = r;
	}

	public float getG() {
		return g;
	}

	public void setG(float g) {
		this.g = g;
	}

	public float getB() {
		return b;
	}

	public void setB(float b) {
		this.b = b;
	}

	public void setA(float a) {
		this.a = a;
	}

	public float getA() {
		return a;
	}

	public void setRGB(float[] rgb) {
		r = rgb[0];
		g = rgb[1];
		b = rgb[2];
		if (rgb.length > 3)
			a = rgb[3];
	}

	public void setRGB(float r, float g, float b) {
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public void setRGB(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public void setRGB(Color color) {
		float[] rgb = color.getRGBComponents(null);
		r = rgb[0];
		g = rgb[1];
		b = rgb[2];
		a = rgb[3];
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Color4) {
			Color4 c = (Color4)obj;
			return Math.abs(r - c.r) <= EPSILON && Math.abs(g - c.g) <= EPSILON && Math.abs(b - c.b) <= EPSILON && Math.abs(a - c.a) <= EPSILON;
		}
		return super.equals(obj);
	}

	public static Color4 middle(Color4 a, Color4 b) {
		return new Color4((a.r + b.r) / 2, (a.g + b.g) / 2, (a.b + b.b) / 2, (a.a + b.a) / 2);
	}

	public static Color4 middle(List<Color4> colors) {

		float r = 0, g = 0, b = 0, a = 0;
		int size = 0;

		for (Color4 c : colors) {
			r += c.r;
			g += c.g;
			b += c.b;
			a += c.a;
			size++;
		}

		return new Color4(r / size, g / size, b / size, a / size);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Color<");
		sb.append(r).append(";").append(g).append(";").append(b).append("|").append(a).append(">");
		return sb.toString();
	}

	public static Color4 make(double r, double g, double b, double a) {
		return new Color4((float)r,(float) g,(float) b,(float) a);
	}
	
	public static Color4 randomColor() {
		Random r = new Random();
		return new Color4(r.nextFloat(), r.nextFloat(), r.nextFloat());
	}

	protected static final float FACTOR = 0.7f;

	public static Color4 darker(Color4 ebd) {

		return new Color4(Math.max(ebd.r * FACTOR, 0), Math.max(ebd.g * FACTOR, 0), Math.max(ebd.b * FACTOR, 0));
	}
	
	public static Color4 brighter(Color4 c) {
		return c.brighter();
	}

	public static Color4 askColor(Color4 ebd) {
		Color c = JColorChooser.showDialog(null, "Choose color:", new Color(ebd.r, ebd.g, ebd.b, ebd.a));
		return new Color4(c);
	}

	public static float BRIGHTER = 20;
	public static float DARKER = -20;
	
	public Color4 brighter() {
		float r = this.r;
		float g = this.g;
		float b = this.b;
		float a = this.a;
		
		// while(turn < -360) turn += 360;
		
		// convert to HSV
		final float max = Math.max(Math.max(r, g), b);
		final float min = Math.min(Math.min(r, g), b);
		final float delta = max - min;
		
		float t;
		if(max == min) {
			t = 0;
		}
		else if(max == r) {
			t = (60 * ((g-b)/delta) + 360)%360;
		}
		else if(max == g) {
			t = (60 * ((b-r)/delta) + 120);
		}
		else {
			t = (60 * ((r - g)/delta) + 240);
		}
		
		float s = (max == 0)? 0 : 1 - (min/max);
		
		float v = max;
		
		// lighter
		// t = (t+turn + 360)%360;
		t = (t+BRIGHTER)%360;
		
		
		// retour a rgb
		int ti =  ((int)(t/60.f))%6;
		float f = t/60.f - ti;
		float l = v*(1 -s);
		float m = v*(1 - f *s);
		float n = v*(1 - (1 -f)*s);
		switch(ti) {
		case 0: return new Color4(v,n,l,a);
		case 1: return new Color4(m,v,l,a);
		case 2: return new Color4(l,v,n,a);
		case 3: return new Color4(l,m,v,a);
		case 4: return new Color4(n,l,v,a);
		case 5: return new Color4(v,l,m,a);
		default: return new Color4(this);
		}
		
	}

	public Color toColor() {
		return new Color(r, g, b, a);
	}
	
	public Color4 toGray() {
		float g = 0.299f * r + 0.587f * this.g + 0.114f * b;
		return new Color4(g, g, g, a);
	}
	
	

	public void scale(float d) {
		this.r *= d;
		this.g *= d;
		this.b *= d;
		
	}
	
	
	public void scaleAdd(double r) {
		this.r += (float)r;
		this.g += (float)r;
		this.b += (float)r;
	}

	public void scale(double d) {
		scale((float)d);
	}

	public Color4 brighter(float i) {
		float backup = BRIGHTER;
		BRIGHTER = i;
		Color4 c = brighter();
		BRIGHTER = backup;
		return c;
	}

	@Override
	public int compareTo(Color4 o) {
		//double res = (norm2() - o.norm2());
		//if(Math.abs(res) <= Point3.EPSILON) {
		double resx = (r - o.r);
		if(Math.abs(resx) <= EPSILON) {
			double resy = (g - o.g);
			if(Math.abs(resy) <= EPSILON) {
				double resz = (b - o.b);
				if(Math.abs(resz) <= EPSILON) {
					double resa = (a - o.a);
					if(Math.abs(resa) <= EPSILON)
						return 0;
					else
						return signum(resa);
				}
				else
					return signum(resz);
			}
			else
				return signum(resy);  
		}
		else
			return signum(resx);
		//}
	//return signum(res);
	}


	private int signum(double resz) {
		if(resz < 0)
			return -1;
		else
			return 1;
	}
	
}
