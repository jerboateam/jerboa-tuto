package jerboa.tuto.ebds;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

import up.jerboa.core.JerboaDart;
import up.jerboa.exception.JerboaException;


/**
 * 
 * @author Hakim BELHAOUARI 
 *
 */

public class Vec3 implements Comparable<Vec3> {
	public static final double EPSILON = 1e-3;
	public static final double sqrt2=Math.sqrt(2.0);
	public static final double sqrt1_2=1.0/sqrt2;

	protected double x,y,z;
	
	/**
	 * Calcul le centre du cercle passant par a,b et c. Attention la fonction suppose que les 3 points ne sont pas
	 * alignes (aucune verification n'est faite dessus).
	 * @param a: premier point
	 * @param b: second point
	 * @param c: troisieme point
	 * @return Le centre du cercle passant par a,b et c.
	 */
	public static Vec3 CentreCercle3Point(Vec3 a, Vec3 b, Vec3 c )
	{
		// on travaille en 2d (X et Y)

		double dxba = b.x - a.x;
		double dyba = b.y - a.y;
		double dxca = c.x - a.x;
		double dyca = c.y - a.y;

		double la2 = dxba * dxba + dyba * dyba;
		double lb2 = dxca * dxca + dyca * dyca;

		double denominator = 1 / (2 * (dxba * dyca - dxca * dyba));// si zero
																	// colinaire

		double centerx = a.x - (dyba * lb2 - dyca * la2) * denominator;
		double centery = a.y + (dxba * lb2 - dxca * la2) * denominator;

		return new Vec3(centerx, centery, 0);
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Vec3) {
			Vec3 p = (Vec3)o;
			return ((Math.abs(p.x-x)<=EPSILON)&&(Math.abs(p.y-y)<=EPSILON)&&(Math.abs(p.z-z)<=EPSILON));
		}
		return false;
	}
	
	public Vec3(int x, int y, int z) {
		this((double)x, (double)y,(double)z);
	}
	public Vec3(double x,double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	public Vec3() {
		this(0,0,0);
	}
	public Vec3(Vec3 a, Vec3 b) {
		this(b.x - a.x, b.y - a.y, b.z - a.z);
	}
	
	public Vec3(Vec3 rhs) {
		this(rhs.x,rhs.y,rhs.z);
	}
	

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}
	
	public Vec3 add(Vec3 p) {
		x+=p.x;
		y+=p.y;
		z+=p.z;
		return this;
		//Vec3 res = new Vec3(x+p.x, y+p.y, z+p.z);
		//return res;
	}
	
	public Vec3 sub(Vec3 p) {
		x-=p.x;
		y-=p.y;
		z-=p.z;
		return this;
		//Vec3 res = new Vec3(x-p.x, y-p.y, z - p.z);
		//return res;
	}
	
	public Vec3 subConst(Vec3 p) {
		Vec3 res = new Vec3(x-p.x, y-p.y, z - p.z);
		return res;
	}
	
	public Vec3 scale(double v) {
		x *= v;
		y *= v;
		z *= v;
		return this;
	}
	
	public Vec3 scale(double sx, double sy, double sz) {
		x *= sx;
		y *= sy;
		z *= sz;
		return this;
	}
	
	public Vec3 scale(Vec3 coefs) {
		x *= coefs.x;
		y *= coefs.y;
		z *= coefs.z;
		return this;
	}
	
	public double dot(Vec3 p) {
		return (x*p.x + y*p.y + z*p.z);
	}
	
	public Vec3 cross(Vec3 v) {
		Vec3 res = new Vec3(
				y*v.z - z*v.y,
				z*v.x - x*v.z,
				x*v.y - y*v.x
				);
		return res;
	}
	
	public double norm() {
		return Math.sqrt(x*x + y*y + z*z);
	}
	
	public double norm2() {
		return (x*x + y*y + z*z);
	}
	
	
	public void normalize() {
		double n = norm();
		if (n != 0.0) {
			scale(1.0/n);
		}
	}
	
	public double distance(Vec3 p) {
		double dx = p.x - x;
		double dy = p.y - y;
		double dz = p.z - z;
		return Math.sqrt(dx*dx + dy*dy + dz*dz);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("<");
		sb.append(x).append(";").append(y).append(";").append(z).append(">");
		return sb.toString();
	}
	
	
	public static Vec3 middle(List<Vec3> points) {
		Vec3 res = new Vec3();
		if(points.size() == 0)
			return res;
		for (Vec3 p : points) {
			res.add(p);
		}
		res.scale(1.0/points.size());
		return res;
	}
	public static Vec3 sum(List<Vec3> points) {
		Vec3 res = new Vec3();
		for (Vec3 p : points) {
			res.add(p);
		}
		return res;
	}
	
	public static Vec3 middle(List<JerboaDart> darts, String ebdname) {
		return middle(darts.stream().map(dart -> (Vec3)dart.ebd(ebdname)).collect(Collectors.toList()));
	}
	
	public static Vec3 sum(List<JerboaDart> darts, String ebdname) {
		return sum(darts.stream().map(dart -> (Vec3)dart.ebd(ebdname)).collect(Collectors.toList()));
	}
	
	public static Vec3 middle(Collection<Vec3> points) {
		Vec3 res = new Vec3();
		if(points.size() == 0)
			return res;
		for (Vec3 p : points) {
			res.add(p);
		}
		res.scale(1.0/points.size());
		return res;
	}
	
	public static Vec3 middle(Vec3... points) {
		Vec3 res = new Vec3();
		if(points.length == 0)
			return res;
		for (Vec3 p : points) {
			res.add(p);
		}
		res.scale(1.0/points.length);
		return res;
	}
	
	public static Vec3 sum(Vec3... points) {
		Vec3 res = new Vec3();
		for (Vec3 p : points) {
			res.add(p);
		}
		return res;
	}
	
	public static Vec3 barycenter(Vec3 a, Number ca, Vec3 b, Number cb) {
		ArrayList<Vec3> al = new ArrayList<Vec3>();
		ArrayList<Number> coef = new ArrayList<Number>();
		al.add(a);al.add(b);
		coef.add(ca); coef.add(cb);
		return barycenter(al, coef);
		
	}
	
	public static Vec3 barycenter(Vec3 a, Number ca, Vec3 b, Number cb,Vec3 c, Number cc) {
		ArrayList<Vec3> al = new ArrayList<Vec3>();
		ArrayList<Number> coef = new ArrayList<Number>();
		al.add(a);al.add(b);al.add(c);
		coef.add(ca); coef.add(cb);coef.add(cc);
		return barycenter(al, coef);
	}
	
	public static Vec3 barycenter(Vec3 a, Number ca, Vec3 b, Number cb,Vec3 c, Number cc,Vec3 d, Number cd) {
		ArrayList<Vec3> al = new ArrayList<Vec3>();
		ArrayList<Number> coef = new ArrayList<Number>();
		al.add(a);al.add(b);al.add(c);al.add(d);
		coef.add(ca); coef.add(cb);coef.add(cc);coef.add(cd);
		return barycenter(al, coef);
	}
	
	public static Vec3 barycenter(List<Vec3> points, List<? extends Number> coefs) {
		Vec3 res = new Vec3();
		double sum = 0.0;
		int l = points.size();
		for(int i = 0; i < l; i++) {
			Vec3 t = new Vec3(points.get(i));
			final double tmp = coefs.get(i).doubleValue(); 
			t.scale(tmp);
			res.add(t);
			sum += tmp;
		}
		if(sum == 0)
			return new Vec3();
		res.scale(1.0/sum);
		return res;
	}
	
	
	
	public static Vec3 askVec3(String message,Vec3 defaut) {
		String p = JOptionPane.showInputDialog(message, defaut);
		Pattern pattern = Pattern.compile("[^+-0123456789]*([-+]?[0-9]+([.][0-9]+)?((e|E)[-+]?[0-9]+)?)");
	
		double[] tab = new double[3];
		int pos = 0;
		Matcher matcher = pattern.matcher(p);
		while(matcher.find() && pos < 3) {
			String number = matcher.group(1);
			tab[pos++] = Double.parseDouble(number);
		}
		Vec3 res = new Vec3(tab[0], tab[1], tab[2]);
		return res;
	}
	
	public static double askDouble(String message, double defaut) {
		String p = JOptionPane.showInputDialog(message, defaut);
		Pattern pattern = Pattern.compile("[^+-0123456789]*([-+]?[0-9]+([.][0-9]+)?((e|E)[-+]?[0-9]+)?)");
		boolean found = false;
		double res = defaut;
		Matcher matcher = pattern.matcher(p);
		while(matcher.find() && !found) {
			String number = matcher.group(1);
			try {
				res = Double.parseDouble(number);
				found = true;
			}
			catch(Exception e) {
				
			}
		}
		return res;
	}
	
	public static int askInteger(String message, int defaut) {
		String p = JOptionPane.showInputDialog(message, defaut);
		Pattern pattern = Pattern.compile("[^+-0123456789]*([-+]?[0-9]+)");
		boolean found = false;
		int res = defaut;
		Matcher matcher = pattern.matcher(p);
		while(matcher.find() && !found) {
			String number = matcher.group(1);
			try {
				res = Integer.parseInt(number);
				found = true;
			}
			catch(Exception e) {
				
			}
		}
		return res;
	}
	
	public static Vec3 extractVector(JerboaDart a, JerboaDart b) {
		Vec3 p = a.<Vec3>ebd("point");
		Vec3 q = b.<Vec3>ebd("point");
		
		return new Vec3(p,q);
	}
	public static boolean isColinear(Vec3 an, Vec3 bn) {
		
		Vec3 a = new Vec3(an);
		a.normalize();
		Vec3 b = new Vec3(bn);
		b.normalize();
		
		
		
		Vec3 v = a.cross(b);
		return (v.norm() <= EPSILON);
	}

	public static void main(String args[]) {
		Vec3 res = askVec3("Bonjour ", new Vec3(5,3e3,-9));
		System.out.println("Obtenu: ["+res+"]");
	}
	
	public static Vec3 rotation(Vec3 init) {
		Vec3 res = askVec3("Axe de rotation: ", new Vec3(0,1,0));
		res.normalize();
		
		String p = JOptionPane.showInputDialog("Angle (degre): ", 0);
		double rot = Double.parseDouble(p);
		double rad = (rot * Math.PI)/180.0;
		double c = Math.cos(rad);
		double s = Math.sin(rad);
		
		double mat[] = new double[9];
		mat[0] = (res.x * res.x) + (1 - (res.x*res.x))* c;
		mat[1] = (res.x * res.y)*(1 - c) - res.z*s;
		mat[2] = (res.x*res.z)*(1 - c) + res.y * s;
		
		mat[3] = res.x*res.y*(1 - c) + res.z*s;
		mat[4] = (res.y*res.y) + (1 - (res.y*res.y))*c;
		mat[5] = res.y * res.z *(1-c) - res.x*s;
		
		mat[6] = res.x*res.z*(1-c) - res.y*s;
		mat[7] = res.y*res.z*(1-c) + res.x*s;
		mat[8] = res.z*res.z + (1- (res.z*res.z))*c;
		
		Vec3 r = new Vec3(init.x, init.y, init.z);
		
		r.x = init.x*mat[0] + init.y*mat[1] + init.z*mat[2];
		r.y = init.x*mat[3] + init.y*mat[4] + init.z*mat[5];
		r.z = init.x*mat[6] + init.y*mat[7] + init.z*mat[8];
		
		return r;
	}
	public static Vec3 rotationY(Vec3 init,double sinteta,double costeta) {
		
		Vec3 r = new Vec3(init.x*costeta +  init.z*sinteta, init.y,init.z*costeta - init.x*sinteta);
		
		
		
		return r;
	}
	
	public static Vec3 rotation(Vec3 init, Vec3 vector, double rad) {
		vector.normalize();
		
		double c = Math.cos(rad);
		double s = Math.sin(rad);
		
		double mat[] = new double[9];
		mat[0] = (vector.x * vector.x) + (1 - (vector.x*vector.x))* c;
		mat[1] = (vector.x * vector.y)*(1 - c) - vector.z*s;
		mat[2] = (vector.x*vector.z)*(1 - c) + vector.y * s;
		
		mat[3] = vector.x*vector.y*(1 - c) + vector.z*s;
		mat[4] = (vector.y*vector.y) + (1 - (vector.y*vector.y))*c;
		mat[5] = vector.y * vector.z *(1-c) - vector.x*s;
		
		mat[6] = vector.x*vector.z*(1-c) - vector.y*s;
		mat[7] = vector.y*vector.z*(1-c) + vector.x*s;
		mat[8] = vector.z*vector.z + (1- (vector.z*vector.z))*c;
		
		Vec3 r = new Vec3(init.x, init.y, init.z);
		
		r.x = init.x*mat[0] + init.y*mat[1] + init.z*mat[2];
		r.y = init.x*mat[3] + init.y*mat[4] + init.z*mat[5];
		r.z = init.x*mat[6] + init.y*mat[7] + init.z*mat[8];
		
		return r;
	}
	
	public static boolean sameList(List<Vec3> pointsA,List<Vec3> pointsB) throws JerboaException {
		return pointsA.equals(pointsB);
		//boolean rA = pointsA.stream().map(p -> pointsB.contains(p)).reduce(true, Boolean::logicalAnd);
		//boolean rB = pointsB.stream().map(p -> pointsA.contains(p)).reduce(true, Boolean::logicalAnd);
		//return rA && rB;
		// return pointsA.containsAll(pointsB) && pointsB.containsAll(pointsA);		
	}
	
	public static Vec3 computeOLDNormal(JerboaDart dart, String ebdpoint) {
		final Vec3 a = dart.ebd(ebdpoint);
		final Vec3 b = dart.alpha(0).ebd(ebdpoint);
		final Vec3 c = dart.alpha(0).alpha(1).alpha(0).ebd(ebdpoint);
		
		Vec3 ab = new Vec3(a,b);
		Vec3 bc = new Vec3(b,c);
		return ab.cross(bc);
	}
	
	/**
	 * Permet de calculer l'angle entre deux vecteurs 3D autour d'un vecteur de base reprÃ©senter par <b>this</b>
	 * @param orient
	 * @param vvect
	 * @return renvoie l'angle (en radian) entre les deux vecteurs en argument par rapport au vecteur courant.
	 */
	public double angle(Vec3 a, Vec3 b) {
		double f = a.dot(b);
		double theta1 = (double)Math.acos(clamp(f/ (a.norm() * b.norm()), -1.0f,1.0f)); 
			
	
		
		double or2 = determinant(this, a, b);
		if(or2 >= 0)
			return theta1;
		else
			return ((2*Math.PI) - theta1);

	}
	
	/**
	 * Permet de calculer l'angle entre deux vecteurs 3D autour d'un vecteur de base reprÃ©senter par <b>this</b>
	 * @param orient
	 * @param vvect
	 * @return renvoie l'angle (en radian) entre les deux vecteurs en argument par rapport au vecteur courant.
	 */
	public double angleUnsigned(Vec3 a, Vec3 b) {
		double f = a.dot(b);
		double theta1 = (double)Math.acos(clamp(f/ (a.norm() * b.norm()), -1.0f,1.0f)); 
		return theta1;
		
	}
	
	public static double determinant(Vec3 a, Vec3 b, Vec3 c) {
		return (a.x * b.y * c.z)
				+ (b.x * c.y * a.z)
				+ (c.x * a.y * b.z)
				- (a.x*c.y*b.z)
				-(b.x*a.y*c.z)
				-(c.x*b.y*a.z);
	}
	
	public static double clamp(double val, double min, double max) {
		return Math.max(min, Math.min(val, max));
	}
	
	public static Vec3 computeNormal(JerboaDart face, String point) {
		Vec3 res = new Vec3(0,0,0);
		if(! (Boolean)face.ebd("orient"))
			face = face.alpha(1);
		JerboaDart cur = face;
		do {
			Vec3 pcur = new Vec3(cur.<Vec3>ebd(point));
			Vec3 pnext = new Vec3(cur.alpha(0).<Vec3>ebd(point));
			
			res.x += (pcur.y - pnext.y) * (pcur.z + pnext.z);
			res.y += (pcur.z - pnext.z) * (pcur.x + pnext.x);
			res.z += (pcur.x - pnext.x) * (pcur.y + pnext.y);			
			
			cur = cur.alpha(0).alpha(1);
		} while(cur != face);
		res.normalize();
		return res;
	}
	
	
	/**
	 * Check if the current point is inside the segment defined by [a;b]
	 * @param a first extremity of the segment
	 * @param b second extremity of the segment
	 * @return Returns true if the current point is inside the segment [a;b], false otherwise.
	 */
	public boolean isInside(Vec3 a, Vec3 b) {
		Vec3 ac = new Vec3(a, this);
		Vec3 cb = new Vec3(this, b);
		Vec3 ab = new Vec3(a,b);
		
		double dist1 = ac.norm() + cb.norm();
		double dist2 = ab.norm();
		
		return ac.dot(cb) >= 0 && ( Math.abs(dist1 - dist2) <= EPSILON);
	}

	/**
	 * Check if the current point is inside box defined [a;b]
	 * @param a corner of the box.
	 * @param b other corner of the box.
	 * @return 
	 */
	public boolean isInsideVol(Vec3 a, Vec3 b) {
		double minX = Math.min(a.x, b.x);
		double minY = Math.min(a.y, b.y);
		double minZ = Math.min(a.z, b.z);
		
		double maxX = Math.max(a.x, b.x);
		double maxY = Math.max(a.y, b.y);
		double maxZ = Math.max(a.z, b.z);
		
		return (minX <= x && x <= maxX) && (minY <= y && y <= maxY) && (minZ <= z && z <= maxZ);
	}

	public boolean isInsideZoneXZ(Vec3 a, Vec3 b) {
		double minX = Math.min(a.x, b.x);
		double minZ = Math.min(a.z, b.z);
		
		double maxX = Math.max(a.x, b.x);
		double maxZ = Math.max(a.z, b.z);
		
		return (minX <= x && x <= maxX) && (minZ <= z && z <= maxZ);
	}

	@Override
	public int compareTo(Vec3 o) {
		//double res = (norm2() - o.norm2());
		//if(Math.abs(res) <= Vec3.EPSILON) {
			double resx = (x - o.x);
			if(Math.abs(resx) <= Vec3.EPSILON) {
				double resy = (y - o.y);
				if(Math.abs(resy) <= Vec3.EPSILON) {
					double resz = (z - o.z);
					if(Math.abs(resz) <= Vec3.EPSILON)
						return 0;
					else
						return signum(resz);
				}
				else
					return signum(resy);  
			}
			else
				return signum(resx);
		//}
		//return signum(res);
	}
	

	private int signum(double resz) {
		if(resz < 0)
			return -1;
		else
			return 1;
	}
	
	
	 /**
     * 
     * 
     * Paul Bourke ( http://local.wasp.uwa.edu.au/~pbourke/geometry/lineline3d/ )
     * http://paulbourke.net/geometry/pointlineplane/
     * Calcul la ligne qui est le chemin le plus court entre deux ligne. 
     * Renvoie faux si aucune solution existe.
     */
    public static boolean trouveSegmentPluscourtDroiteDroite(Vec3 a, Vec3 b, Vec3 c, Vec3 d,
                                            Vec3 pa, Vec3 pb,
                                            double[] theResult) {

        final Vec3 ca =  new Vec3(c,a);
        final Vec3 cd = new Vec3(c,d);
        if (Math.abs(cd.x) <= EPSILON && Math.abs(cd.y) <= EPSILON && Math.abs(cd.z) <= EPSILON) {
            return false;
        }

        final Vec3 ab = new Vec3(a,b);
        if (Math.abs(ab.x) <= EPSILON && Math.abs(ab.y) <= EPSILON && Math.abs(ab.z) <= EPSILON) {
            return false;
        }

        final double d1343 = (ca.x * cd.x) + (ca.y * cd.y) + (ca.z * cd.z); // ca.cd
        final double d4321 = (cd.x * ab.x) + (cd.y * ab.y) + (cd.z * ab.z); // cd.ab
        final double d1321 = (ca.x * ab.x) + (ca.y * ab.y) + (ca.z * ab.z); // ca.ab
        final double d4343 = (cd.x * cd.x) + (cd.y * cd.y) + (cd.z * cd.z); // cd.cd
        final double d2121 = (ab.x * ab.x) + (ab.y * ab.y) + (ab.z * ab.z); // ab.ab

        final double denom = (d2121 * d4343) - (d4321 * d4321);
        if (Math.abs(denom) <= EPSILON) {
            return false;
        }
        final double numer = (d1343 * d4321) - (d1321 * d4343);

        final double mua = numer / denom;
        final double mub = (d1343 + (d4321 * mua)) / d4343;

        pa.x = a.x + (mua * ab.x);
        pa.y = a.y + (mua * ab.y);
        pa.z = a.z + (mua * ab.z);
        pb.x = c.x + (mub * cd.x);
        pb.y = c.y + (mub * cd.y);
        pb.z = c.z + (mub * cd.z);

        if (theResult != null) {
            theResult[0] = mua;
            theResult[1] = mub;
        }
        return true;
    }


	
	
	/**
	 * Returns the intersection point of two lines defined by (a;b) and (c;d). If no value or an error occur
	 * the function return null pointer.
	 * @param a first point on first line
	 * @param b second point on first line
	 * @param c first point on second line
	 * @param d second point on second line
	 * @return Returns the intersection point of two lines or null pointer otherwise.
	 */
	public static Vec3 intersectionDroiteDroite(Vec3 a,Vec3 b, Vec3 c, Vec3 d) {
		Vec3 pa = new Vec3(0,0,0);
		Vec3 pb = new Vec3(0,0,0);
		
		boolean res = trouveSegmentPluscourtDroiteDroite(a, b, c, d, pa, pb, null);
		if(res && pa.equals(pb))
			return pa;
		else
			return null;
	}
	
	public static Vec3 intersectionSegmentSegment(Vec3 a,Vec3 b, Vec3 c, Vec3 d) {
		Vec3 res = intersectionDroiteDroite(a, b, c, d);
		if(res != null && res.isInside(a, b) && res.isInside(c, d))
			return res;
		else
			return null;
	}
	
	/**
	 * Calcul de l'intersection entre un segment [a;b] et un plan decris par un point du plan nomme c et 
	 * comme normal le vecteur n. Le resultat est stocke dans le dernier argument out.
	 * renvoie vrai si le calcul est reussit et faux sinon.
	 * @param a debut du segment
	 * @param b fin du segment
	 * @param c un point du plan
	 * @param n normal du plan 
	 * @param out resultat 
	 * @return renvoie vrai si le calcul est reussit et faux sinon.
	 */
	public static boolean intersectionPlanSegment(Vec3 a, Vec3 b, Vec3 c, Vec3 n, Vec3 out) {
		
		Vec3 ac = new Vec3(a,c);
		Vec3 ab = new Vec3(a,b);
		
		double denum = n.dot(ab);
		
		double num = n.dot(ac);
		
		if(denum  == 0) { // parallele ou sur le plan. 
			return false;
		}
		
		double u = num/denum;
		out.x = a.x + (u * ab.x);
		out.y = a.y + (u * ab.y);
		out.z = a.z + (u * ab.z);
		
		
		if(-Vec3.EPSILON <= u && u <= (1.+Vec3.EPSILON))
			return true;
		else
			return false;
	}
	
	/**
	 * Calcul de l'intersection entre une droite passant par [a;b] et un plan decris par un point du plan nomme c et 
	 * comme normal le vecteur n. Le resultat est stocke dans le dernier argument out.
	 * renvoie vrai si le calcul est reussit et faux sinon.
	 * @param a premier point de la droite
	 * @param b second point de la droite
	 * @param c un point du plan
	 * @param n normal du plan 
	 * @param out resultat 
	 * @return renvoie vrai si le calcul est reussit et faux sinon.
	 */
	public static boolean intersectionPlanDroite(Vec3 a, Vec3 b, Vec3 c, Vec3 n, Vec3 out) {
		
		Vec3 ac = new Vec3(a,c);
		Vec3 ab = new Vec3(a,b);
		
		double denum = n.dot(ab);
		
		double num = n.dot(ac);
		
		if(Math.abs(denum) <= EPSILON) { // parallele ou sur le plan. 
			return false;
		}
		
		double u = num/denum;
		out.x = a.x + (u * ab.x);
		out.y = a.y + (u * ab.y);
		out.z = a.z + (u * ab.z);
		
		return true;
	}
	
//	public static boolean intersectionOrthogonalPolygonConvexLine(List<Vec3> pts, Vec3 n, Vec3 a, Vec3 out) {
//		Vec3 b = new Vec3(a);
//		b.add(n);
//		boolean res = intersectionPlanDroite(a, b, pts.get(0), n, out);
//		
//		if(res) {
//			
//		}
//		return res;
//	}
//	
	
	/**
	 * This function compute if the point a is inside a polygon with the normal n
	 * 
	 * @param polygon list of consecutive vertex
	 * @param n normal of the polygon
	 * @param a point to test if inside the polygon
	 * @return return true if the point a is inside the polygon, false else.
	 */
	public static boolean insidePolygon(List<Vec3> polygon, Vec3 n,  Vec3 a) {
		Vec3 b = middle(polygon);
		int j = 0;
		int size = polygon.size();
		while(j < polygon.size() && a.equals(b)) {
			b = middle(polygon.get(j), polygon.get((j+1)%size));
			j++;
		}
		
		Vec3 ab = new Vec3(a,b);
		
		if(Math.abs(ab.dot(n)) > EPSILON) {
			return false;
		}
		
		int intersection = 0;
		for(int i = 0;i < size; i++) {
			Vec3 pa = polygon.get(i);
			Vec3 pb = polygon.get((i+1)%size);
			
			Vec3 c = intersectionDroiteDroite(a, b, pa, pb);
			if(c != null)
				intersection++;
			
		}
		
		return (intersection%2) == 1;
	}
	
	
	/*
	 * equation de la ligne apres verif du guard est:
	 *  p = c1 N1 + c2 N2 + u N1*N2 
	 *  
	 */
	public static boolean intersectionPlanePlane(Vec3 p1, Vec3 n1, Vec3 p2, Vec3 n2, Vec3 output, Vec3 dir) {
		final double d1 = -d(p1,n1);
		final double d2 = -d(p2,n2);
		
		Vec3 guard = n1.cross(n2);
		if(guard.norm2() < EPSILON)
			return false;
		
		final double n1n1 = n1.dot(n1);
		final double n2n2 = n2.dot(n2);
		final double n1n2 = n1.dot(n2);
		
		final double det = n1n1*n2n2 - n1n2*n1n2;
		
		final double c1 = (d1*n2n2 - d2*n1n2)/det;
		final double c2 = (d2*n1n1 - d1*n1n2)/det;
		
		// droite def par 
		// c1*N1 + c2*N2 + t* cross(N1,N2);
		// on renvoie le point tq t = 0
		
		output.x = c1*n1.x + c2*n2.x;
		output.y = c1*n1.y + c2*n2.y;
		output.z = c1*n1.z + c2*n2.z;
		
		dir.setPoint(guard);
		
		return true;
	}
	
	private void setPoint(Vec3 p) {
		x = p.x;
		y = p.y;
		z = p.z;
	}

	/**
	 * Renvoie le plus proche du point C appartenant a la droite (AB). 
	 * Erreur si A et B sont confondus
	 * @param a: point de la droite
	 * @param b: point de la droite
	 * @param c: point en dehors de la droite
	 * @return Renvoie le plus proche de C appartenant a la droite (AB).
	 */
	public static Vec3 closestPoint(Vec3 a,Vec3 b, Vec3 c) {
		Vec3 ab = new Vec3(a, b);
		
		Vec3 ac = new Vec3(a,c);
		double k = ac.dot(ab) / ab.dot(ab);
		Vec3 p = new Vec3(a.x + (k *ab.x),a.y + (k *ab.y),a.z + (k *ab.z) );
		
		return p;
	}

	

	/**
	 * Function that complete the equation of a plane defined by a point P and its normal N.
	 * The equation has the form ax+by+xz+d = 0. The computation takes into consideration
	 * that P.N = -d.
	 * @param p point in the plane
	 * @param normal normal of the plane
	 * @return Return 
	 */
	public static double d(Vec3 p, Vec3 normal) {
		double d = -(normal.x * p.x + normal.y *p.y + normal.z*p.z);
		return d;
	}
	

	public static Vec3 min(Vec3 a, Vec3 b) {
		Vec3 r = new Vec3(
				Math.min(a.x, b.x),
				Math.min(a.y, b.y),
				Math.min(a.z, b.z)
				);
		return r;
	}
	
	public static Vec3 max(Vec3 a, Vec3 b) {
		Vec3 r = new Vec3(
				Math.max(a.x, b.x),
				Math.max(a.y, b.y),
				Math.max(a.z, b.z)
				);
		return r;
	}

	public Vec3 addConst(Vec3 n) {
		return new Vec3(x+n.x, y+n.y, z+n.z);
	}
	
	
	public static Vec3 linear(Vec3 a, Vec3 b, float k) {
		return new Vec3(a.x + k * (b.x - a.x), a.y + k * (b.y - a.y), a.z + k * (b.z - a.z));
	}
	
	
	public static Vec3 computeNewellMethod(JerboaDart face) {
		Vec3 res = new Vec3(0,0,0);
		JerboaDart cur = face;
		try {
		do {
			Vec3 pcur = new Vec3(cur.<Vec3>ebd("pos"));
			Vec3 pnext = new Vec3(cur.alpha(0).<Vec3>ebd("pos"));
			
			res.x += (pcur.y - pnext.y) * (pcur.z + pnext.z);
			res.y += (pcur.z - pnext.z) * (pcur.x + pnext.x);
			res.z += (pcur.x - pnext.x) * (pcur.y + pnext.y);			
			
			cur = cur.alpha(0).alpha(1);
		} while(cur != face);
		res.normalize();
		return res;
		}
		catch(RuntimeException re) {
			System.err.println("ERROR DURING computation of Normal Newell Method on dart: "+face);
			throw re;
		}
	}
	
}
