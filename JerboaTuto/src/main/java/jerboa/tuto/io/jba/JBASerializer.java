package jerboa.tuto.io.jba;

import java.util.List;
import java.util.StringTokenizer;

import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.ebds.Color4;
import jerboa.tuto.ebds.Vec3;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaEmbeddingInfo;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaOrbit;
import up.jerboa.exception.JerboaException;
import up.jerboa.exception.JerboaRuntimeException;
import up.jerboa.util.serialization.EmbeddingSerializationKind;
import up.jerboa.util.serialization.jba.JBAEmbeddingSerialization;

public class JBASerializer implements JBAEmbeddingSerialization {

	private JerboaTutoModeler modeler;

	public JBASerializer(JerboaTutoModeler modeler) {
		this.modeler = modeler;
	}

	@Override
	public EmbeddingSerializationKind kind() {
		return EmbeddingSerializationKind.SAVEANDLOAD;
	}

	@Override
	public boolean manageDimension(int dim) {
		return (dim == 3);
	}

	@Override
	public JerboaEmbeddingInfo searchCompatibleEmbedding(String ebdname, JerboaOrbit orbit, String type) {
		if(ebdname.equals("color") && orbit.equals(modeler.getColor().getOrbit()))
			return modeler.getColor();
		else if(("point".equals(ebdname) || "position".equals(ebdname)|| "pos".equals(ebdname) || "posPlie".equals(ebdname) || "foldPoint".equals(ebdname)) && orbit.equals(modeler.getPos().getOrbit()))
			return modeler.getPos();
		else if("orient".equals(ebdname) && orbit.equals(modeler.getOrient().getOrbit()))
			return modeler.getOrient();
		else if("normal".equals(ebdname) && orbit.equals(modeler.getNormal().getOrbit()))
			return modeler.getNormal();
		else
			return null;
	}

	@Override
	public void completeProcess(JerboaGMap gmap, List<JerboaDart> created) throws JerboaException {
		// ATTENTION trigger qui se lance en fin de chargement avec l'ensemble
		// des brins ajoutes.

	}

	@Override
	public CharSequence serialize(JerboaEmbeddingInfo info, Object value) {
		switch (info.getName()) {
		case "point":
		case "posPlie":
			Vec3 p = (Vec3) value;
			return "" + p.getX() + " " + p.getY() + " " + p.getZ();
		case "color":
		case "facecolor":
			Color4 c = (Color4) value;
			return "" + c.getR() + " " + c.getG() + " " + c.getB() + " " + c.getA();
		case "orient":
			Boolean b = (Boolean) value;
			return "" + b.toString();
		case "normal":
			Vec3 n = (Vec3)value;
			return "" + n.getX() + " " + n.getY() + " " + n.getZ();
		default:
			throw new JerboaRuntimeException("Unknown embedding in serialization!");
		}
	}

	@Override
	public Object unserialize(JerboaEmbeddingInfo info, String ebdline) { // ByteArrayInputStream
																			// toto
																			// =
																			// new
																			// ByteArrayInputStream(ebdline.getBytes());

		StringTokenizer token = new StringTokenizer(ebdline);
		switch (info.getName()) {
		case "posPlie":
		case "point": {
			double x = Double.parseDouble(token.nextToken());
			double y = Double.parseDouble(token.nextToken());
			double z = Double.parseDouble(token.nextToken());

			return new Vec3(x, y, z);
		}
		case "orient": {
			return Boolean.parseBoolean(token.nextToken());
		}
		case "normal": {
			double x = Double.parseDouble(token.nextToken());
			double y = Double.parseDouble(token.nextToken());
			double z = Double.parseDouble(token.nextToken());

			return new Vec3(x, y, z);
		}
		case "color":
		case "facecolor": {
			float x = Float.parseFloat(token.nextToken());
			float y = Float.parseFloat(token.nextToken());
			float z = Float.parseFloat(token.nextToken());
			float w = Float.parseFloat(token.nextToken());

			return new Color4(x, y, z, w);
		}
		default:
			throw new JerboaRuntimeException("Unknown embedding in serialization!");
		}
	}

}
