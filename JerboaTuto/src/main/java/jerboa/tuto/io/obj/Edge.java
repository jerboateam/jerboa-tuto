package jerboa.tuto.io.obj;

import up.jerboa.core.JerboaDart;

public class Edge implements Comparable<Edge> {
	public Face owner;
	protected int start;
	protected int end;
	protected JerboaDart dartStart;
	protected JerboaDart dartEnd;
	
	public Edge(Face owner,int a, int b, JerboaDart da,JerboaDart db) {
		this.owner = owner;
		this.start = a;
		this.end = b;
		this.dartStart = da;
		this.dartEnd = db;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Edge) {
			Edge d = (Edge) obj;
			return ((start == d.start) || (end == d.start))
					&& ((start == d.end) || (end == d.end));
		}
		return false;
	}	

	
	@Override
	public String toString() {
		return "<"+start+" -- " + end+">";
	}

	public int min() { return Math.min(start, end); }
	public int max() { return Math.max(start, end); }

	@Override
	public int compareTo(Edge o) {
		int r = Integer.compare(min(), o.min());
		if(r == 0) {
			r = Integer.compare(max(), o.max());
			return r;
		}
		else
			return r;
	}
}
