package jerboa.tuto.io.obj;

import java.util.Comparator;

import jerboa.tuto.ebds.Vec3;


public class EdgeComparator implements Comparator<Edge> {
	private OBJLoader owner;
	private Vec3 refnorm;
	private Vec3 vec;

	public EdgeComparator(OBJLoader objImporter, Edge base) {
		owner = objImporter;
		refnorm = base.owner.geonorm;
		Vec3 a = owner.getPoint(base.start-1);
		Vec3 b = owner.getPoint(base.end-1);
		
		vec = new Vec3(a, b);
	}

	@Override
	public int compare(Edge e0, Edge e1) {
		double a = vec.angle(refnorm, e0.owner.geonorm);
		double b = vec.angle(refnorm, e1.owner.geonorm);
		return Double.compare(a, b);
	}

}
