package jerboa.tuto.io.obj;

import up.jerboa.core.JerboaDart;

public class FacePart {

	public int vindex;
	public int tindex;
	public int nindex;
	public String mindex; // mtl
	
	public JerboaDart edge0;
	public JerboaDart edge1;
	
	
	public FacePart(int vindex, int tindex, int nindex, String mindex) {
		this.vindex = vindex;
		this.tindex = tindex;
		this.nindex = nindex;
		this.mindex = mindex;
		
		edge0 = null;
		edge1 = null;
	}
	
	
	public boolean sameVertex(FacePart p) {
		return (vindex == p.vindex);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof FacePart) {
			FacePart fp = (FacePart)obj;
			return fp.vindex == vindex;
		}
		return super.equals(obj);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("(");
		sb.append(vindex);
		sb.append(")");
		return sb.toString();
	}
}
