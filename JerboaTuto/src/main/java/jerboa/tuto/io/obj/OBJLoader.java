package jerboa.tuto.io.obj;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.ebds.Color4;
import jerboa.tuto.ebds.Vec3;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.util.Pair;
import up.jerboa.util.StopWatch;

public class OBJLoader {
	private JerboaGMap gmap;

	private ArrayList<Vec3> points;
	private ArrayList<Vec3> normals;
	private ArrayList<Vec3> texcoords;
	private ArrayList<String> materials;
	private ArrayList<String> mtllibs;
	
	private ArrayList<Face> groups;
	
	private transient String curmat;
	private int ebdOrientID;
	private int ebdPointID;
	private int ebdColorID;
	private int ebdNormID;
	
	private Color4 color;
	
	
	
	public OBJLoader(JerboaTutoModeler modeler) {
		this.gmap = modeler.getGMap();
		this.points = new ArrayList<>();
		this.normals = new ArrayList<>();
		this.texcoords = new ArrayList<>();
		this.materials = new ArrayList<>();
		this.mtllibs = new ArrayList<>();
		
		ebdOrientID = modeler.getOrient().getID();
		ebdPointID = modeler.getPos().getID();
		ebdColorID = modeler.getColor().getID();
		ebdNormID = modeler.getNormal().getID();
		
		color = new Color4(0.3f,0.3f,0.9f);
		
		groups = new ArrayList<>();
	}
	
	public List<String> parse(InputStream objfile) {
		curmat = null;
		Scanner reader = new Scanner(objfile);
		reader.useLocale(Locale.ENGLISH);
		long start = System.currentTimeMillis();
		int lino = 0;
		while (reader.hasNext()) {
			lino++;
			String cmd = reader.next();
			if (!cmd.trim().isEmpty()) {
				if (cmd.startsWith("vn"))
					parseNormal(reader);
				else if (cmd.startsWith("vt"))
					parseTex(reader);
				else if (cmd.startsWith("v")) // attention derniers des v
					parseVertex(reader);
				else if (cmd.startsWith("f"))
					parseFaceV2(reader,curmat);
				else if (cmd.startsWith("mtllib")) {
					final String mtllib = reader.nextLine();
					mtllibs.add(mtllib);
				} else if (cmd.startsWith("usemtl")) {
					curmat = reader.nextLine();
					materials.add(curmat);
				}
				else if(cmd.startsWith("l")) {
					reader.nextLine(); // polyline
				}
				else if(cmd.startsWith("g") || cmd.startsWith("o")) {
					reader.nextLine();
					closePrevObj();
				}
				else if(cmd.startsWith("#")){
					reader.nextLine();
				} else {
					String line = reader.nextLine();
					System.out.println("Unsupported line("+lino+"): " + cmd + " " + line);
				}
			}
		}
		closePrevObj();
		long end = System.currentTimeMillis();
		System.out.println("End parsing in : " + (end-start) + " ms.");
		return mtllibs;
	}
	

	private void closePrevObj() {
		System.err.println("New group/object detected. Number of faces: " + groups.size());
		if(groups.size() > 0) {
			
			StopWatch sw = new StopWatch();
			sw.display("closing group/object...");

			Map<Edge, List<Edge>> maps = groups.stream().flatMap(face -> face.getEdges().stream()).parallel()
				.collect(Collectors.toMap(
					e -> { return e; },
					e ->  { List<Edge> edges = new ArrayList<Edge>(); edges.add(e); return edges; }, 
					(a, b) -> { List<Edge> r = new ArrayList<Edge>(a); r.addAll(b); return r; }, 
					TreeMap::new
				)
			);
			
			sw.display("end of repartition of edges");
			Pair<Integer,Integer> res = maps.values().parallelStream().map(edges -> {
				final int size = edges.size();
				if(size == 2) {
					Edge e0 = edges.get(0);
					Edge e1 = edges.get(1);
					JerboaDart left = null, right = null;
					if(e0.start == e1.start) {
						left  = e0.dartStart;
						right = e1.dartStart;
					}
					else {
						left  = e0.dartStart;
						right = e1.dartEnd;
					}
					left.setAlpha(2, right);
					left.alpha(0).setAlpha(2, right.alpha(0));
					return new Pair<Integer, Integer> (1,0);
				}
				else
					return new Pair<Integer, Integer> (0,1);
				
			}).reduce((a,b) -> new Pair<>(a.l() + b.l(), a.r() + b.r())).orElse(new Pair<Integer,Integer>(-1,-1));
			
			if(res.l() == -1) {
				sw.display("end on an error :'(");
			}
			else {
				sw.display("end successful: count of edge of deg 2 = " + res.l() + ", others = " + res.r());
			}
			
			groups = new ArrayList<>();
		}
	}

	private void parseFaceV2(Scanner reader,String curmat) {
		String line = reader.nextLine();
		// GROUP 1 2 3 4 5
		Pattern patternFace = Pattern.compile("(\\d+)(/(\\d*))?(/(\\d*))?");
		Matcher matcher = patternFace.matcher(line);

		ArrayList<FacePart> face = new ArrayList<FacePart>();
		
		while (matcher.find()) {
			int vindex = -1;
			int tindex = -1;
			int nindex = -1;
			String svindex = matcher.group(1);
			String stindex = matcher.group(3);
			String snindex = matcher.group(5);
			vindex = Integer.parseInt(svindex);
			if (stindex != null && !stindex.isEmpty()) {
				tindex = Integer.parseInt(stindex);
			}
			if (snindex != null && !snindex.isEmpty()) {
				nindex = Integer.parseInt(snindex);
			}
			FacePart fp = new FacePart(vindex, tindex, nindex, curmat);
			face.add(fp);
		}
		if (face.size() >= 3) {
			makeFace(face);
		} else {
			System.out.println("DEGENEREE FACE: " + face);
			
		}
	}

	private void parseNormal(Scanner reader) {
		double x = reader.nextDouble();
		double y = reader.nextDouble();
		double z = reader.nextDouble();
		reader.nextLine();
		Vec3 n = new Vec3(x, y, z);
		normals.add(n);
	}

	private void parseTex(Scanner reader) {
		double x = reader.nextDouble();
		double y = reader.nextDouble();
		reader.nextLine();
		Vec3 t = new Vec3(x, y, 0);
		texcoords.add(t);
	}
	

	private void parseVertex(Scanner reader) {
		double x = reader.nextDouble();
		double y = reader.nextDouble();
		double z = reader.nextDouble();
		reader.nextLine();
		Vec3 p = new Vec3(x, y, z);
		points.add(p);
	}
	
	
	private void makeFace(List<FacePart> face) {
		final int fsize = face.size();
		
		
		JerboaDart[] nodes = gmap.addNodes(fsize * 2);
		for (int i = 0; i < fsize; i++) {
			face.get(i).edge0 = nodes[i * 2];
			face.get(i).edge1 = nodes[(i * 2) + 1];
			
			nodes[i * 2].setAlpha(0, nodes[(i * 2) + 1]);
			nodes[i * 2].setEmbedding(ebdOrientID, Boolean.TRUE);
			nodes[(i * 2) + 1].setAlpha(1,
					nodes[((i + 1) * 2) % nodes.length]);
			nodes[(i * 2) + 1].setEmbedding(ebdOrientID, Boolean.FALSE);
			FacePart part1 = face.get(i);
			FacePart part2 = face.get((i+1)%fsize);
			fixEbd(part1, nodes[i * 2]);
			fixEbd(part2, nodes[(i * 2) + 1]);
		}
		
		Vec3 geonorm = Vec3.computeNewellMethod(nodes[0]);
		for (JerboaDart dart : nodes) {
			dart.setEmbedding(ebdNormID, geonorm);
		}
		Face f = new Face(this,face);
		f.geonorm = geonorm;
		groups.add(f);
	}

	private void fixEbd(FacePart part, JerboaDart dart) {
		dart.setEmbedding(ebdPointID, points.get(part.vindex-1));
		dart.setEmbedding(ebdColorID, color);
		
		if(part.nindex > 0)
			dart.setEmbedding(ebdNormID, normals.get(part.nindex-1));
	}
	

	public Vec3 getPoint(int index) {
		return points.get(index - 1);
	}
	
	public Vec3 getNormal(int index) {
		return normals.get(index - 1);
	}
}
