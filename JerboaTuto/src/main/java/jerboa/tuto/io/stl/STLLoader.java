package jerboa.tuto.io.stl;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import fr.up.xlim.sic.ig.jerboa.trigger.tools.JerboaMonitorInfo;
import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.ebds.Color4;
import jerboa.tuto.ebds.Vec3;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaGMap;

public class STLLoader {

	// "solid "
	
	private JerboaTutoModeler modeler;
	private JerboaGMap gmap;
	
	private int ebdpointid;
	private int ebdnormalid;
	private int ebdorientid;
	private int ebdcolorid;
	
	private Color4 color;
	
	private transient List<STLEdge> created;
	
	
	public STLLoader(JerboaTutoModeler modeler) {
		this.modeler = modeler;
		this.gmap = modeler.getGMap();
		
		ebdpointid = modeler.getPos().getID();
		ebdnormalid = modeler.getNormal().getID();
		ebdorientid = modeler.getOrient().getID();
		ebdcolorid = modeler.getColor().getID();
		
		color = Color4.LIGHTGRAY;
	}

	
	public void load(File file, JerboaMonitorInfo worker) throws IOException {
		
		FileReader rfile = new FileReader(file);
		BufferedReader reader = new BufferedReader(rfile);
		created = new ArrayList<>();
		
		String header = reader.readLine();
		reader.close(); rfile.close();
		if(header.startsWith("solid"))
			loadText(file, worker);
		else
			loadBinary(file, worker);
		
		/*created.sort((a,b) -> {
			Vec3 pa = (Vec3) a.getEmbedding(ebdpointid);
			Vec3 pb = (Vec3) b.getEmbedding(ebdpointid);
			return pa.compareTo(pb);
		});*/
		
		
		created.sort((a,b) -> { return a.compareTo(b); });
		for(int i = 0;i < created.size()-1; ++i) {
			STLEdge e1 = created.get(i);
			STLEdge e2 = created.get(i+1);
			
			if(e1.equals(e2) && e1.d1.isFree(2) && e2.d1.isFree(2)) {
				e1.d1.setAlpha(2, e2.d1);
				e1.d2.setAlpha(2, e2.d2);
			}
		}
	}
	
	class STLEdge implements Comparable<STLEdge> {
		JerboaDart d1,d2;
		Vec3 a,b;
		
		public STLEdge(JerboaDart d1,JerboaDart d2) {
			this.d1 = d1;
			this.d2 = d2;
			a = (Vec3) d1.getEmbedding(ebdpointid);
			b = (Vec3) d2.getEmbedding(ebdpointid);
			if(a.compareTo(b) > 0) {
				Vec3 c = a;
				a = b;
				b = c;
				this.d1 = d2;
				this.d2 = d1;
			}
		}

		@Override
		public int compareTo(STLEdge o) {
			int r = a.compareTo(o.a);
			if(r == 0) {
				int s = b.compareTo(o.b);
				return s;
			}
			else 
				return r;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof STLEdge) {
				STLEdge nn = (STLEdge) obj;
				int r = a.compareTo(nn.a);
				int s = b.compareTo(nn.b);
				return r == 0 && s == 0;
				
			}
			return false;
		}
		
		@Override
		public String toString() {
			return a.toString()+"--"+b.toString();
		}
		
	}


	private void loadBinary(File file, JerboaMonitorInfo worker) throws IOException {
		System.out.println("Loading STL in binary mode...");
		worker.setMessage("Loading STL in binary mode...");
		byte[] headers = new byte[80];
		int size = 0;
		
		FileInputStream fis = new FileInputStream(file);
		try(DataInputStream dis = new DataInputStream(fis)) {
			dis.read(headers); // on ignore l'entete 
			
			{
				byte[] bsize = new byte[4];
				dis.read(bsize);
				ByteBuffer bbsize = ByteBuffer.wrap(bsize);
				bbsize.order(ByteOrder.LITTLE_ENDIAN);
				size = bbsize.asIntBuffer().get();
			}
			worker.setMinMax(0, size);
			worker.setProgressBar(0);
			
			for(int i = 0;i < size; ++i) 
			{
				worker.setProgressBar(i);
				float nx,ny,nz;
				
				byte[] triangle = new byte[50];
				dis.read(triangle);
				
				ByteBuffer btriangle = ByteBuffer.wrap(triangle);
				btriangle.order(ByteOrder.LITTLE_ENDIAN);
				FloatBuffer ftriangle =  btriangle.asFloatBuffer();
				
				nx = ftriangle.get(); ny = ftriangle.get(); nz = ftriangle.get();
				Vec3 normal = new Vec3(nx, ny, nz);
				
				nx = ftriangle.get(); ny = ftriangle.get(); nz = ftriangle.get();
				Vec3 v1 = new Vec3(nx, ny, nz);
				
				nx = ftriangle.get(); ny = ftriangle.get(); nz = ftriangle.get();
				Vec3 v2 = new Vec3(nx, ny, nz);
				
				nx = ftriangle.get(); ny = ftriangle.get(); nz = ftriangle.get();
				Vec3 v3 = new Vec3(nx, ny, nz);
				
				List<Vec3> points = new ArrayList<>(3);
				points.add(v1);points.add(v2);points.add(v3);
				
				makeFace(normal, points);
			}
		}
		
		
	}


	private void loadText(File file, JerboaMonitorInfo worker) throws IOException {
		System.out.println("Loading STL in text mode...");
		worker.setMessage("Loading STL in text mode...");
		
		FileReader in = new FileReader(file);
		try(BufferedReader br = new BufferedReader(in)) {
			String line;
			Vec3 normal = null;
			List<Vec3> points = new ArrayList<>();
			while((line = br.readLine()) != null) {
				line = line.trim();
				if(line.startsWith("facet")) {
					StringTokenizer tokenizer = new StringTokenizer(line);
					tokenizer.nextToken(); // facet
					if(tokenizer.hasMoreTokens()) {
						tokenizer.nextToken(); // normal
						String sx = tokenizer.nextToken(); float x = Float.parseFloat(sx);
						String sy = tokenizer.nextToken(); float y = Float.parseFloat(sy);
						String sz = tokenizer.nextToken(); float z = Float.parseFloat(sz);
						normal = new Vec3(x, y, z);
					}
				}
				
				if(line.startsWith("vertex")) {
					StringTokenizer tokenizer = new StringTokenizer(line);
					tokenizer.nextToken(); // vertex
					if(tokenizer.hasMoreTokens()) {
						String sx = tokenizer.nextToken(); float x = Float.parseFloat(sx);
						String sy = tokenizer.nextToken(); float y = Float.parseFloat(sy);
						String sz = tokenizer.nextToken(); float z = Float.parseFloat(sz);
						points.add(new Vec3(x, y, z));
					}
				}
				
				if(line.startsWith("endfacet")) {
					makeFace(normal,points);
					normal = null;
					points.clear();
				}
				
				if(line.startsWith("endsolid")) {
					
				}
				
			} // end while
		}
	}


	private void makeFace(Vec3 normal, List<Vec3> points) {
		
		JerboaDart[] darts = gmap.addNodes(points.size() * 2);
		
		for(int i =0;i < points.size(); ++i) {
			darts[(i*2)+0].setAlpha(1, darts[(i*2)+1]);
			darts[(i*2)+1].setAlpha(0, darts[((i*2)+2)%darts.length]);
			
			darts[(i*2)+0].setEmbedding(ebdpointid, points.get(i));
			darts[(i*2)+1].setEmbedding(ebdpointid, points.get(i));
			
			darts[(i*2)+0].setEmbedding(ebdnormalid, normal);
			darts[(i*2)+1].setEmbedding(ebdnormalid, normal);
			
			darts[(i*2)+0].setEmbedding(ebdorientid, true);
			darts[(i*2)+1].setEmbedding(ebdorientid, false);
			
			darts[(i*2)+0].setEmbedding(ebdcolorid, color);
			darts[(i*2)+1].setEmbedding(ebdcolorid, color);
		}
		for(int i =0;i < points.size(); ++i) {
			created.add(new STLEdge(darts[(i*2)+1], darts[((i*2)+2)%darts.length]));
		}
	}
}
