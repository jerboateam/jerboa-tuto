package jerboa.tuto.launcher;

import javax.swing.JFrame;

import fr.up.xlim.sic.ig.jerboa.viewer.GMapViewer;
import jerboa.tuto.JerboaTutoModeler;
import jerboa.tuto.bridge.JTBridgeTuto;
import up.jerboa.exception.JerboaException;

public class JTLauncher {
	
	public static void main(String[] args) throws JerboaException {
		
		JFrame frame = new JFrame("Jerboa Tutorial");
		
		JerboaTutoModeler modeler = new JerboaTutoModeler();
		JTBridgeTuto bridge = new JTBridgeTuto(modeler);
		GMapViewer viewer = new GMapViewer(frame, modeler, bridge);
	
		
		frame.add(viewer);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.pack();
		frame.setVisible(true);
	}

}
