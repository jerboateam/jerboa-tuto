package jerboa.tuto.util;

import java.util.ArrayList;

import jerboa.tuto.ebds.Vec3;

/**
 * This class is an ease for collecting all Vec3 objects in scripts as generic type is not 
 * fully supported in Jerboa.
 * 
 * @author Hakim
 *
 */
public class Vec3List extends ArrayList<Vec3>{

	/**
	 * Generated serial UID by eclipse
	 */
	private static final long serialVersionUID = -5242349360869563238L;

}
