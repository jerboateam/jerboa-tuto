# Langage d'expression et de script en Jerboa

[Retour à la page d'accueil](README.md)

Cette page est dédiée aux personnes ayant déjà une compréhension de Jerboa et de la structure sous-jacente des cartes généralisées ([vous pouvez vous rendre à cette page si besoin](PresentationJerboa.md)).

L'écriture des expressions de plongement en Jerboa se fait dans un langage conçu par Valentin Gauthier dans le cadre de sa [thèse à XLIM (disponible ici)](https://www.theses.fr/2019POIT2252). Elle contient des opérateurs spécifiques aux cartes généralisées et des variables particulières selon le contexte utilisé.

## Sommaire:
* [Généralités](#généralités)
* [Typage](#typage)
* [Séquence](#séquence)
* [Conditionnelle](#conditionnelle)
* [Boucles](#boucles)
* [Déclaration](#déclarations)
* [Appel d'une opération Jerboa](#appel-dune-opération-jerboa)
* [Expressions](#expression)
* [Opérateurs d'expression spécifiques à Jerboa](#opérateurs-dexpression-spécifiques-à-jerboa)
* [Variables spécifiques à Jerboa](#variables-spécifiques-à-jerboa)
* [Commandes spécifiques à Jerboa](#commandes-spécifiques-à-jerboa)

---------------------------------

### Généralités

L'intérêt de ce langage est de profiter des constructions spécifiques dans le domaine des cartes généralisées et de l'architecture de Jerboa, qui permet de fournir des vérifications mêlant topologie et calcul de plongement.

Le langage Jerboa est inspiré du Java7 (avant les JavaStreams) et donc on retrouve les éléments syntaxiques de ce langage. Les expressions usuelles du langage Java, ainsi que les opérateurs usuels sont présents.


### Typage

Tous les types peuvent être utilisés dans les expressions de plongement ou les scripts. Certains types internes de Jerboa sont aussi autorisés:

- **JerboaDart**: classe des brins. Les opérateurs spécifiques de Jerboa s'applique sur les objets de ce type et permet le parcours de la structure des cartes généralisées. La récupération des valeurs de plongement.
- **JerboaDarts**: représente une liste de brins.
- **JerboaRuleResult**: représente un résultat d'une application de règle Jerboa. Les résultats sont représentés sous la forme d'une matrice où les colonnes représentent les noeuds du motif droit et les lignes sont les représentants de ces noeuds.
- **List<#Type>**: représente une liste homogène d'élément **#Type**. Attention ce type est retraduit en interne.
- **\@ebd<#ebdname>**: utilisé en guise de type, il correspond au type du plongement **#ebdname**
> Exemple: **``@ebd<pos>``** représente le type des positions (par exemple la classe Vec3).


---------------------------------
### Séquence
Le séquencement d'instructions  suit les conventions des langages C ou Java:
```
#instruction1;
#instruction2;
```
où ...

---------------------------------
### Conditionnelle

Les instructions conditionnelles suivent les conventions des langages C ou Java:


```
if(#condition)
    #instruction
else
    #instruction
```
et la version sans branche "else"
```
if(#condition)
    #instruction
```
avec les éléments suivants:
- ``#condition``: expression booléenne
- ``#instruction``:  une instruction ou plusieurs instructions

-------------------------------
### Boucles

Le langage  inclut trois types de boucles : la boucle _while_  et la boucle la boucle _for_ et la boucle d'itération _foreach_ sur des collections itérables 

```
while(#condition) {
    #instruction
}
```
avec les éléments suivants:
- ``#condition``: expression booléenne
- ``#instruction``: une ou plusieurs instructions

```
for (#type #var : #start..#end) {
    #instruction
}
```
avec les éléments suivants:
- ``#type``: type de la variable d'itération
- ``#var``: nom de la variable d'itération
- ``#start``: la valeur initiale
- ``#end``: la valeur finale (incluse)
- ``#instruction``: une ou plusieurs instructions

```
foreach(#type #var : #coll) {
    #instruction
}
```
avec les éléments suivants:
- ``#type``: type de la variable d'itération
- ``#var``: nom de la variable d'itération
- ``#coll``: nom de la variable représentant une collection
- ``#instruction``: une ou plusieurs instructions
---------------------------------
### Déclarations

La déclaration de variables locales répond à l'une des deux formes suivantes:

- **Type a;**  introduit la variable **a** de type **Type** sans initialisation.
- **Type a = #expr;** introduit la variable **a** de type **Type** initilisée avec l'expression **#expr**.

---------------------------------
### Appel d'une opération Jerboa

L'appel d'une autre opération Jerboa ne peut se faire que dans les _scripts_ Jerboa. La syntaxe est de la forme:
```
@rule<#ruleName>(#hook1,...,#hookN,#pl1,...,#plM)
```
avec les éléments suivants:
- ``#ruleName``: nom court d'une règle Jerboa.
- ``#hook1,...,#hookN``: paramètres topologiques sous forme de brin.
- ``#pl1,...,#plM``: paramètres de plongement sous forme d'instance d'objet.
- N et M : nombre de paramètres de la règle Jerboa #ruleName

---------------------------------
### Expressions

Les expressions sont analogues aux expressions des langages C et Java, à l'exception des streams. Elles sont construites à partir des éléments suivants:

- les littéraux: variables de type entier, flottant ou chaîne de caractère
- les opérations booléennes: and, or, not
- les opérations de comparaison: <, <=, >, >=, ==, !=
- les opérations arithmétiques: +, -, *, /
- les opérations d'incrémentation ou de décrémentation: ++ et -- (en notation postfixe)

---------------------------------
### Opérateurs d'expression spécifiques à Jerboa

 Le langage possède des constructions dédiées aux structures des cartes généralisées:

- **#brin\@#dim**:  le brin voisin de **#brin**  via l'arc  **#dim**
> Exemple: **d\@0\@2\@0\@2** est le brin atteint en suivant successivements les arcs **0 2 0 2** à partir de **d**.
- ``<#orb>``: orbite décrite par la liste de dimensions  **#orb** dont les éléments sont des dimnensions et le séparateur est la virgule "," 
> Exemple1: **<0,1>(d)** renvoie tous les brins de face 2D à partir du brin **d**<br>
> Exemple2: **<0,1,2>(d)** renvoie tous les brins du volume à partir du brin **d**
- ``<#orb>_#plong``: l'opérateur de collecte renvoie une liste de valeur de plongement (identifié par son nom **#plong**) pour une orbite donnée (ici **#orb**).
> Exemple1: **<0,1,2>_position(d)** renvoie la liste des positions du volume représenté par le brin d.
- ``<#orb1>_<#orb2>``: la collecte permet de récupérer une liste de brin sur l'orbite **#orb1** en prenant qu'un seul représentant par orbite **#orb2**
> Exemple1: **<0,1,2>_<1,2,3>** renvoie un unique brin par sommet pour un volume

---------------------------------
### Variables spécifiques à Jerboa

Selon que vous soyez dans une expression de plongement ou un script, vous avez la possibilité d'accéder à des variables globales:

- **\@modeler**: permet d'accéder au modeleur en cours d'utilisation.
- **\@gmap**: permet de récupérer la carte généralisée en cours de modification.
- **\@dimension**: constante indiquant la dimension de travail.
- **\@ebd<#ebdname>.name**: récupère le nom du plongement définit dans le modeleur.
- **\@ebd<#ebdname>.orbit**: récupère l'orbite du plongement définit dans le modeleur.
- **hooks**: représente une matrice des brins donnés en entrée d'un _SCRIPT_ Jerboa uniquement. Ainsi, **hooks.dart(#col)** renvoie le premier brin du hook numéroté **#col**. Une autre variante **hooks.dart(#col,#row)** renvoie le brin d'entrée pour le **#col**-eme hook de la règle et son **#row**-eme représentant (Cette forme est pour les _SCRIPT_ Jerboa). 
> Exemple: **hooks.dart(0,0)** récupère le premier hook et plus précisément son premier représentant.

---------------------------------
### Commandes spécifiques à Jerboa

Le langage inclut des commandes particulières dépendantes du langage cible.

- **\@header(#lang) { #code }**: cette structure permet d'ajouter le code **#code** en  tête du code généré pour le langage **#lang**.
> Exemple:<br>
>```
> @header(java) {
>    import java.util.ArrayList;
>} 
>```
> Permet d'ajouter dans la génération en Jerboa Java, le code d'importation de la classe **ArrayList**.

- **\@lang(#lang) { #code }**: cette structure permet d'injecter le code natif **#code** du langage cible **#lang** pour compenser le manque de construction particulière dans le langage Jerboa.
> Exemple:<br>
> ```
> JerboaDarts darts;
> @lang(java) {
>   darts = darts.stream().map(d -> d.alpha(0)).collect(Collectors.toList());
> }
> ```
> Ce code exploite les streams Java pour la structure de liste pour faire un calcul particulier.

--------------------------------


[Retour à la page d'accueil](README.md)
