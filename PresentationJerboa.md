[Retour à la page d'accueil](README.md)

# Présentation de Jerboa

Jerboa est une suite logicielle dédiée à la modélisation géométrique à base topologique, dont la spécificité réside dans la représentation des données et des opérations. En effet, le formalisme de graphe permet de représenter à la fois la structure des objets et les opérations pouvant s'appliquer sur ces objets en utilisant les transformations de graphes.

Le diagramme ci-dessous résume l'ensemble des outils permettant de définir un modeleur complet réalisé avec l'outil Jerboa:

<p style="text-align : center">
    <img src="images/diagramJerboaUpdated.svg" />
</p>

Les zones en vertes représentent les outils ou bibliothèques de Jerboa à savoir:
- l'éditeur : qui permet de concevoir ou modifier des opérations et offre des mécaniques de vérifications statiques.
- le noyau Jerboa : qui contient l'API de jerboa pour utiliser le formalisme de graphe.
- le vieweur générique : qui offre un interface simple en 3D de tous les modeleurs réalisés en Jerboa. Il offre ainsi une mécanique simple pour appliquer des transformations ou alors des fonctionnalités communes à tous les modeleurs Jerboa.

Les zones bleues représentent des entités obtenues par programmation dans le langage cible:
- La spécification de nos modeleurs.
- Les calculs des plongements au sein des règles créées.
- Les bibliothèques pour représenter les plongements sous forme de classes.
- Le noyau de modeleur généré, qui contient une version code des règles réalisées dans l'éditeur et d'autre élément de code que vous jugerez utile (comme par exemple la gestion d'un format de fichier particulier).
- Le Bridge qui permet de faire le pont entre le modeleur nouvellement créé et le vieweur générique.



## Le noyau de modeler et représentation fondamentale

Avant de décrire l'API de Jerboa, il faut comprendre le formalisme des cartes généralisées, la notion de plongement et les transformations de graphe.

### Formalisme des cartes généralisées (gmaps)

Les cartes généralisées permettent de représenter des quasi-variétés. Ce terme permet de représenter les objets qui vérifie une propriété d'adjacence entre les cellules topologique (les cellules 0D représentent les sommets, les cellules 1D sont les arêtes, les cellules 2D pour les faces et les cellules 3D pour les volumes, ...) Ainsi deux cellules de dimension *i* doivent être adjacente à une même cellule de dimension *i-1*.

> **Exemple:** deux faces voisines partagent une arête commune.

La définition formelle d'une carte généralisée sous la forme d'un graphe se présente donc de la façon suivante:
            
> Soit n la dimension topologique des objets à modéliser. Le graphe $G = (V,E,s,t,\alpha)$ où :
> - $V$ : ensemble des brins
> - $E$ : ensemble des arcs
> - $s : E \rightarrow V$ et $t : E \rightarrow V$ sont les fonctions sources et cible des arcs de notre graphe
> - $\alpha : E \rightarrow [0,n]$ est la fonction d'étiquetage des arcs par la dimension
>
> > **Notation :** on note $\alpha_i : V \rightarrow V$ la fonction qui renvoie le voisin d'un brin le long de l'arc de dimension *i*. En d'autre terme : soient $(v,w) \in V^2, \alpha_i(v) = w$ revient à $\exists e \in E, s(e) = v \land t(e) = w \land \alpha(v) = i$
>
> Ainsi ce graphe est une carte généralisée si et seulement si le graphe vérifie les contraintes de cohérence suivantes:
> - non-orientation : $\forall e \in E, \exists! e' \in E, s(e) = t(e') \land t(e) = s(e') \land \alpha(e) = \alpha(e')$
> - arcs adjacents : $\forall i \in [0,n], \forall v \in V, \exists (e_1,e_2) \in E^2, (\alpha(e_1) = i \land s(e_1) = v \land \alpha(e_2) = i \land s(e_2) = v) \Rightarrow e_1 = e_2 $
> - condition de cycle: $\forall v \in V, \forall (i,j) \in [0,n]^2, \alpha_i(\alpha_j(\alpha_i(\alpha_j((v))))) = v$

Une intuition pour comprendre la structuration d'une carte généralisée est d'imaginer un objet selon les cellules de dimension décroissante. Ainsi, la figure ci-dessous montre une surface 2D composée d'un carré surplombé d'un triangle qui forme l'exemple de la maison. Nous partons donc des deux formes géométriques de dimension 2 et elles sont adjacentes à une même arête connecté par un $\alpha_2$ (en bleu sur la seconde figure).

(continuer le schéma, mais fait-on une figure une explication ? ou toutes les explications puis les figures les uns à la suite des autres?)

### Le formalisme des plongements


### Les opérations en Jerboa : les transformations de graphes

Dans Jerboa, les opérations sont exprimées comme des transformations de graphes particulières. Les transformations de graphe généralisent la réécriture de termes à des structures non-linéaires. L'approche utilisée est celle par double somme amalgamée (connue sous le nom "double pushout" -- DPO -- dans la littérature). Dans ce formalisme, une règle est composée de trois graphes qui représentent le motif supprimé, préservé et ajouté lors de la transformation. Plus précisément, une règle $p$ se présente alors sous la forme $L \leftarrowtail K \rightarrowtail R$ où les graphes $L$, $K$ et $R$ sont respectivement appelés *membre gauche*, *interface* et *membre droit* de la règle. On impose ici que $K$ soit un sous-graphe de $L$ et de $R$, de sorte que $L$, $K$ et $R$ décrivent respectivement le sous-graphe modifié par $p$, le sous-graphe préservé lors de l'application de $p$ et le graphe ajouté dans le graphe transformé. Intuitivement, l'application d'une règle à un graphe $G$ peut être résumée en trois étapes :

- trouver une occurrence de $L$ dans $G$ (un sous-graphe de $G$ isomorphe à $L$) ;
- supprimer les éléments (les nœuds et les arcs) de $L$ qui ne sont pas dans $K$ ;
- ajouter au graphe les éléments de $R$ qui ne sont pas dans $K$.

Dans une optique pratique, on se contente de règles de la forme $L \to R$ où l'interface $K$ est définie comme l'intersection des deux graphes $L$ et $R$ et laissée implicite. 

Nous avons étendu l'approche DPO des transformations de graphes afin de définir un langage dédié à la manipulation des cartes généralisées. Ainsi, les règles sont paramétrées par la donnée d'un type d'orbite (point, face, volume, etc). Un nœud d'un règle ne représente plus simplement un nœud de la carte généralisée en cours de transformation, mais une orbite toute entière dont le type est donné par l'étiquette attachée au nœud de la règle. Nous obtenons alors un langage simple et de haut niveau pour décrire des modifications considérées comme standards dans le domaine de la modélisation géométrique.

Pour conclure, il est nécessaire de comprendre que la règle doit contenir toute l'information nécessaire à la description de l'opération de modélisation géométrique. Autrement dit, toutes les informations utilisées pour calculer les modifications de l'objet doivent être présentes : topologie comme plongements. Ici, les modifications topologiques sont encodées par les modifications de structure du graphe, tandis que les modifications de plongements sont encodées par des expressions de calcul ajoutées aux nœuds du membre droit de la règle.

**Bibliographie:**

L'ouvrage de référence pour une compréhension détaillée des constructions théoriques sous-jacentes aux transformations de graphes reste probablement le monographe de l'École de Berlin.

1. H. Ehrig, K. Ehrig, U. Prange, and G. Taentzer, Fundamentals of Algebraic Graph Transformation. 2006. doi: [10.1007/3-540-31188-2](https://www.doi.org/10.1007/3-540-31188-2).

Certains tutoriels permettront à un lecteur impatient de comprendre rapidement les constructions et motivations sans utiliser explicitement les notions sous-jacentes liées aux catégories.

1. H. Ehrig, “Introduction to the algebraic theory of graph grammars (a survey)”. 1979. doi: [10.1007/BFb0025714](https://www.doi.org/10.1007/BFb0025714).
2. H. Ehrig, M. Korff, and M. Löwe, “Tutorial introduction to the algebraic approach of graph grammars based on double and single pushouts”. 1991. doi: [10.1007/BFb0017375](https://www.doi.org/10.1007/BFb0017375).
3. B. König, D. Nolte, J. Padberg, and A. Rensink, “A Tutorial on Graph Transformation”. 2018. doi: [10.1007/978-3-319-75396-6_5](https://www.doi.org/10.1007/978-3-319-75396-6_5).

Le livre de R. Heckel et G. Taentzer présente une vision moderne des transformations de graphes à destination de non-experts du domaine.

1. R. Heckel and G. Taentzer, Graph Transformation for Software Engineers: With Applications to Model-Based Development and Domain-Specific Language Engineering. 2020. doi: [10.1007/978-3-030-43916-3](https://www.doi.org/10.1007/978-3-030-43916-3).

Les fondements théoriques de Jerboa, c'est-à-dire l'application de la théorie générale au cadre particulier des cartes généralisées telles qu'utilisées dans Jerboa peut être consultée dans les articles suivants :

1. R. Pascual, P. Le Gall, A. Arnould, and H. Belhaouari, “Topological consistency preservation with graph transformation schemes”. 2022. doi: [10.1016/j.scico.2021.102728](https://www.doi.org/10.1016/j.scico.2021.102728).
2. A. Arnould, H. Belhaouari, T. Bellet, P. L. Gall, and R. Pascual, “Preserving consistency in geometric modeling with graph transformations”. 2022. doi: [10.1017/S0960129522000226](https://www.doi.org/10.1017/S0960129522000226).


## L'éditeur de règles

### Conception d'un modeleur

### Les vérifications statiques

## Le vieweur générique

### Préparation d'un vieweur générique : création du Bridge

### Lancement et manipulation du vieweur.