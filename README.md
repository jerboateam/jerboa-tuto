# Jerboa Tutoriel

-------------------------------------------------------------------------------

## Installation de son projet

Nous avons préparé dans ce dépôt, l'ensemble des ressources pour démarrer votre apprentissage de Jerboa, il vous suffit de ``cloner`` le projet et d'utiliser le projet ``maven`` présent dans le répertoire ``JerboaTuto``.

Si vous avez besoin de plus d'information vous pouvez passer sur la [page décrivant un peu plus en détail l'installation](Installation.md)

-------------------------------------------------------------------------------
## Présentation du modeleur dans ce Tutoriel

Pour ce tutoriel, nous allons démarrer depuis un modeleur minimaliste réalisé avec les outils Jerboa qui se nomme ``JerboaTutoModeler``.

Le modeleur est l'entité Jerboa, qui permet de représenter les objets encodés dans le formalisme des cartes généralisées, ainsi que les opérations pouvant être réalisées.
Nous proposons un modeleur gérant des objets 3D (c'est-à-dire qu'il y a l'existence des liens $\alpha_0$, $\alpha_1$, $\alpha_2$ et $\alpha_3$). Les plongements présents dans ce modeleur sont les suivants:
- `pos` : représente les positions géométriques. Où, les brins de l'orbite $\langle \alpha_1, \alpha_2, \alpha_3 \rangle$ ont les mêmes valeurs.
- `normal` : indique la normale de la face incidente aux brins.
- `color` : représente une couleur au format RGBA par face (orbite $\langle \alpha_0, \alpha_1 \rangle$)
- `orient` : correspond à un booléen qui traduit l'orientation de l'objet. Ainsi, nous pourrons déterminer si l'objet, représenté par la structure de carte généralisé, est orientable ou non.


-------------------------------------------------------------------------------

## Premier lancement d'un modeleur existant

Dans cette partie, nous allons montrer quelques manipulations essentielles du viewer Jerboa. Suivez les étapes de l'installation de ce tutoriel pour comprendre comment lancer le viewer et vous pourrez passer aux étapes suivantes :

1. Dans un premier temps, nous allons vous demander de vous familiariser avec le chargement d'un modèle. Le modeleur proposé permet de charger des objets 3D aux formats OBJ, STL et JBA (interne à Jerboa). 
Cliquez dans le menu sur **File > Load**. Cette action doit ouvrir une fenêtre dans le répertoire ``assets`` (fourni dans le dépôt)et vous  devez charger le modèle ``cube.obj``.

    > Jerboa charge l'objet avec la vue classique en Jerboa à savoir la vue topologique (ou vue éclatée).
Pour passer à la vue géométrique (vue usuelle dans la communauté), il suffit de passer en mode compact par le menu en décochant **View > Exploded view** (et inversement).
    > La vue topologique propose un compromis entre la vue géométrique classique et la vue graphe, car les arcs sont visuels. Il utilise pour cela des calculs barycentrique dont les coefficients peuvent être modifiable par l'utilisateur pour accentuer ou non l'éclatement. Pour cela, cherchez dans la partie inférieur de l'interface, l'onglet **View settings** pour voir les différents curseurs pour jouer avec les poids (_Attention, si l'objet est gros cela peut engendrer beaucoup de calcul_).

![Vue du cube après son chargement](images/tuto_viewer_step1.png "Vue du cube après son chargement")


2. Maintenant sélectionnez un brin quelconque de l'objet en faisant un click gauche sur un brin (ou dans une zone proche d'un brin). Le panneau à droite va afficher le détail du brin : les adjancences pour les différentes dimension ($\alpha_i$) et les valeurs des différents plongements. Ce brin est appelé _hook_ et il représente le point de départ pour appliquer une transformation.

![Sélection d'un brin](images/tuto_viewer_step2.png "Vue après la sélection d'un brin (dans notre exemple il s'agit du brin 47)")

3. Cherchez dans la liste des règles, située à gauche, le répertoire **subdivision** et dépliez-le. Ensuite, sélectionnez l'opération **Menger3D** et cliquez sur le bouton **Apply** en bas pour appliquez l'opération. Bravo, vous avez appliqué une opération avec Jerboa.

![Résultat après l'application de la règle Menger3D](images/tuto_viewer_step3.png "Vue après l'application de la règle Menger3D")

> L'interface de Jerboa propose une caméra orbitale où vous pouvez tourner autour d'une cible en gardant le bouton droit appuyé de la souris dans le canvas 3D. Ainsi, la translation horizontal contrôle l'angle $\theta$ et les mouvements verticaux propse une rotation sur l'angle $\phi$. Les coordonnées de la cible dans la vue sont les coordonnées présente en bas du canevas 3D ou vous pouvez utiliser l'un des boutons ``center view``. Chaque bouton permet de centrer la scène selon le contexte, ainsi celui dans la barre d'outil permet de position au centre géométrique de la scène, celui à droite (au dessus de la présentation du détail des brins) permet de centrer sur le centre de la sélection et enfin le bouton dans le descriptif du brin permet de centrer la vue sur les coordonnées du brin.

---------------------------------------------------------------------------------

<!--
TODO: couler les descriptions avant les exos.

#### Création d'une cellule
présentation création d'un triangle (ou d'arête) en détail!
avec les étapes du modeleur, le 

#### Règle sans modification topologique
Le scale


### Règle d'augmentation

Le schéma de Loop



OBJECTIF: comprendre ce qu'on manipule
TODO: dans les exprs y'a des valeurs mais ya du code (en gros la partie prog)

-->

## Ecriture d'opérations simples

Cette partie repose sur l'utilisation des transformations de graphes pour obtenir des opérations dans Jerboa au travers de son editeur de règles de transformations. Vous pouvez utiliser l'[éditeur](https://gitlab.com/jerboateam/jerboa-tuto/-/raw/main/JerboaTuto/editor/JerboaModelerEditor.jar) présent dans le répertoire ``editor`` du projet [JerboaTuto](https://gitlab.com/jerboateam/jerboa-tuto/-/tree/main/JerboaTuto) (compiler en version Java 11) en lancant le jar présent, ou alors en installant depuis le [dépôt officiel](https://gitlab.com/jerboateam/jerboa-editor)

<!--
Nous nous baserons sur un modeleur 3D nommé ``JerboaTutoModeler``, qui contient les plongements suivants:
* **pos**: la position géométique du sommet - orbite support <a1,a2,a3>
* **normal**: la normale d'une face - orbite support <a0,a1>
* **color**: la couleur d'une face - orbite support <a0,a1>
* **orient**: booléen indiquant l'orientation de l'objet - orbite support <>
-->

Plusieurs opérations usuelles sont données dans le fichier [JerboaTuto.jme](JerboaTuto/src/main/rc/JerboaTuto.jme) présent dans votre dépôt.

### Les règles de création

> **Rappel:**
> Les règles de création sont des règles qui n'ont pas de noeuds sur la partie gauche.
> Si vous voulez appliquer une règle de création vous ne devez pas avoir de brin sélectionné (pas de hook!).
> Ainsi Jerboa ne crée qu'un seul exemplaire de chaque noeud. 

#### Opération de création d'un brin

Commençons par une simple règle qui va créer un seul brin isolé : 
1. Cliquez sur **File > New Atomic Rule**. Dans la nouvelle fenêtre, tapez le nom de la règle à savoir: **CreatDart** et ensuite cliquez sur **OK**. Vous devriez obtenir la vue comme sur l'image ci-dessous qui représente la vue de la règle atomique vide dans Jerboa.

![Fenêtre de la règle nouvellement créée](images/tuto_creatdart_step1.png "Fenêtre de la règle nouvellement créée")

 > Cette fenêtre se décompose de plusieurs parties :
>    - La barre de menu et la barre d'outil en haut.
>    - La zone à gauche qui représente le motif gauche de la règle sous forme d'un graphe qui capture la topologique où on peut appliquer la règle.
>    - La zone à droite qui représente le motif droite de la règle sous forme d'un graphe qui indique comment le motif gauche est transformé pour obtenir l'opération voulue.
>    - Le bas sous forme de plusieurs onglets pour remplir les paramètres de la règle et les expressions de plongements.

2. Modifions un peu les paramètres de notre règle en spécifiant une catégorie à notre règle (assimilable à la notion de module). Pour cela, sélectionnez l'onglet **Details** dans la zone inférieure. Ensuite, dans le champ **Category**, entrez la valeur **creat**. Cliquez sur **Apply** pour confirmer votre choix.

3. Dans la zone à droite, faite un clique droit (sans bouger!) dans le canvas 2D. Cela devrait vous créer un noeud du graphe avec un pictogramme et des mots en noir et d'autre en gris comme sur la figure suivante :

![Création d'un noeud dans la partie droite](images/tuto_creatdart_step3.png "Création d'un brin sur le motif droit")

- le pictogramme indique selon la couleur une alerte (en jaune), une erreur (en rouge) ou un problème sur les plongements (en vert).
- le symbole $\langle \rangle$ représente la variable d'orbite qui permet à Jerboa de manipuler simultanément plusieurs brins à la fois. Ici, nous n'exploiterons pas encore cette mécanique.
- la liste des plongements suit les conventions suivantes:
    * les noms en gris indique l'absence ou la transmission d'une expression de plongement. Dans cet exemple, on transmet les valeurs par défaut du langage Java.
    * les noms sont encadrés par des points d'exclamations si l'expression de plongement est requise!
    * les noms sont entre parenthèse si l'expression de plongement est souhaitée mais qu'une expression par défaut a été détectée. Dans notre exemple, les plongements color et orient renvoie respectivement une couleur aléatoire et l'orientation à faux.

4. Notre objectif sera d'effacer les pictogrammes qui indique des problèmes et qui correspondent aux vérifications à la volée de Jerboa. Pour cela, il faut comprendre les aspects fondamentaux de Jerboa et pour vous aider, les vérifications à la volée peuvent vous donner des indices pour comprendre les soucis. Dans la zone en bas, cliquez sur l'onglet **Errors** pour visualiser les erreurs détectées comme sur la figure ci-dessous. En lisant les messages, l'éditeur indique des erreurs les différents cycles (en accord avec la définition formelle des cartes généralisées).

![Listes des erreurs détectées par l'éditeur](images/tuto_creatdart_step4.png "Listes des erreurs détectées par l'éditeur")

5. Pour faire simple, la déclaration de notre brin n'est pas complet et nous devons mentionner tous les $\alpha_i$. Dans cette situation, notre brin doit être relié à lui-même pour toutes les dimensions (ainsi nous respecterons toutes les contraintes formelles sur les cartes généralisées). Pour cela, faites des cliques droits à l'intérieur du noeud sur le motif droit (sans bouger la souris). L'editeur doit ajouter une boucle à chaque clique en essayant de déterminer les bonnes dimensions.
Lorsque vous aurez ajouté tous les liens, toutes les erreurs disparaitront.

![Ajout des alphas et correction des erreurs](images/tuto_creatdart_step5.png "Ajout des alphas et correction des erreurs")

> Code couleur : vous remarquerez que l'éditeur adopte un code couleur pour chaque dimension des liens alphas :  noir pour les $\alpha_0$, rouge pour les $\alpha_1$, bleu pour les $\alpha_2$ et vert pour les $\alpha_3$. 

6. Maintenant, nous allons spécifier les expressions de plongements qui vont affecter les valeurs de plongement du brin. Pour cela sélectionnez avec un clique gauche le noeud **n0**, celui-ci doit apparaitre en magenta (couleur de la sélection en Jerboa). Au dessus du canevas 2D vous devriez voir un panneau qui vous permet de visualiser le détail du brin.
![Ajout des expressions de plongements](images/tuto_creatdart_step6.png "Sélectionnez et regardez la barre d'outil")

Dans la liste déroulante vous pouvez choisir le plongement où vous devez faire un calcul. Laissez l'entrée commençant par `pos` et cliquez sur le bouton **EDIT**. Un nouvel onglet devrait apparaître en bas de la fenêtre.

7. Dans la zone de texte, vous devez coder dans le [langage Jerboa](Langage.md) le calcul qui doit faire un **return** sur la valeur initial de ce brin. Pour cela, nous allons proposer à l'utilisateur de saisir une position en appelant une fonction particulière de la classe ``Vec3`` qui représente les vecteurs 3D dans notre modeleur. Entrez le code suivant qui cré une position par défaut à l'origine et qui lance une  interface graphique pour la saisie de la position initiale et ensuite cliquez sur le bouton **Apply** :

```
Vec3 tmp = new Vec3(0,0,0);
return Vec3.askVec3("Saisissez un vecteur séparé par des ;", tmp);
```

Vous remarquerez dans le noeud que le mot `pos` est devenu noir (si vous déselectionnez le noeud) et les points d'exclamation ont disparu.

8. En jouant avec le menu déroulant précédemment cité, mettez les codes suivant :

    * Pour le plongement `normal` :
        ```
        return new Vec3(0,0,0);
        ```
    * Pour le plongement `color` :
        ```
        return Color4.randomColor();
        ```
    * Pour le plongement `orient` :
        ```
        return false;
        ```
    

9. Maintenant que nous avons produit notre règle de création de brin, nous allons pouvoir produire le code Java, pour alimenter le modeleur de manière effective dans le visualiseur 3D. Pour cela, vous devez cliquer dans le menu de la fenêtre principale **File > Export Modeler**.
> **Attention**: vous devez rafraichir votre projet soit :
>   * sous Eclipse: clique-droit sur le projet et dans le menu contextuel, choississez `Refresh`.
>   * sous Visual Studio Code: relancer la compilation.

![Génération du code du modeleur](images/tuto_creatdart_step9.png "Génération du code du modeleur")

10. Executez votre projet pour lancer le visualiseur 3D qui contiendra la nouvelle règle sur le menu à gauche. Appliquez la règle que nous venons de créer. Le compteur de brin dans la barre d'état de l'application indique le nombre de brin présent votre carte généralisée actuellement (ce nombre aussi appelé la taille a du passé de 0 à 1).

![Lancement de notre nouvelle règle](images/tuto_creatdart_step10.png "Lancement de notre nouvelle règle CreatDart")

Bravo! vous venez de créer une nouvelle règle Jerboa! Dans la suite nous ferons une opération beaucoup plus visuel qu'un point dans l'espace.


#### Opération de création d'un quad

A présent, réalisons une règle de création d'une face quadrangulaire (nommée quad) pour tirer des relations d'adjacence entre les brins :

![Règle de création d'un quad](images/rule_creatquad.svg "Règle de création d'un quad")

Comme vous le constatez, la règle n'a qu'un motif droit qui représente les huit brins d'un quad où le chainage _a0_, _a1_ est bien respecté. On remarque aussi que les arcs _a2_ et _a3_ forme des boucles ce qui indique que ce quad est isolé (il n'est pas adjacent à une autre cellule).

Concernant, les mots dans chaque noeud (nommée **expression de plongement**) indique la présence d'un calcul pour le plongement mentionné. Ainsi par exemple, le noeud n0 a une expression de plongement pour l'ensemble des plongemens présents dans le modeleur, alors que le noeud n2 possède uniquement un calcul pour le plongement orient (les autres étant induits automatiquement par Jerboa à partir de l'orbite support).

Comme nous voulons créer quelques choses nous devons décider des valeurs de plongements et nous avons décidé les éléments suivants:
- Les noeuds n0 et n7 seront en (0,0,0)
- Les noeuds n1 et n2 seront en (1,0,0)
- Les noeuds n3 et n4 seront en (1,0,1)
- Les noeuds n5 et n6 seront en (0,0,1)
- La face aura une couleur aléatoire.
- La normal sera (0,1,0) pour rester cohérent avec notre géométrie des sommets qui sont dans le plan (xz).
- L'orientation alterne le long du chemin <a0,a1> en partant de n0 a ``true``.

Vous pouvez regarder directement dans l'éditeur comment ses contraintes se traduisent sous la forme d'une expression de plongement.

> [Lien vers l'aide des expressions de plongement](Langage.md) 

### _Exercices_

- En suivant se modèle réalisez la règle qui permet de créer une arête, puis une autre règle qui crée un triangle.

> Conseil: <br>
> Posez vous la question de combien de brins sont concernés par ces opérations de création.
> * Dans un premier temps pensez en terme de nombre d'arête (Pour rappel, vous avez 2 brins par arête).
> * Dans un second temps, pensez aux autres dimensions et leur adjacence sur les arêtes.
> * Enfin, lorsque la topologie est terminée, réaliser les expressions de plongement là où l'éditeur détecte la nécessité d'expression.


--------------------

### Les règles sans modification topologique

Nous avons vu des opérations avec _rien_ dans le membre gauche, mais cela limite beaucoup les opérations réalisables. Nous allons donc voir maintenant comment faire une opération qui permet de manipuler un objet existant. Cependant, nous nous contenterons des opérations qui ne modifie pas la topologie mais uniquement les valeurs de plongement.


Regardez les trois opérations suivantes, on remarque que ces règles ont le même noeud à gauche et à droite (même nom, même orbite) et donc ne modifie pas la topologie. Cependant, on remarque le mot-clé ``pos`` dans le noeud de droite ce qui indique un calcul pour le plongement **pos**. Si vous avez bien compris cette notion d'orbite, essayez de deviner la différence entre elle et l'impact que l'on pourrait observer sur chacune de leur application:

![Translation d'un sommet](images/rule_translate_vertex.svg) 

![Translation d'une composante connexe](images/rule_translate_connex.svg) 

![Translation d'une face](images/rule_translate_face.svg)

donc de prime abord on remarque tout de suite que se sont les orbites dans les noeuds qui change d'une règle à l'autre, ainsi la première permet de translater un sommet, puis toute une composante connexe (possiblement plusieurs volume simultanément) et la dernière translate uniquement une face.

La translation est la modification de la position au travers d'un vecteur de translation et l'expression des trois opérations se réduit au code suivant:
```
Vec3 p = new Vec3(n0.pos);
return p.add(vect);
```
Ce code est identique entre les trois opérations et c'est Jerboa qui appelera ce calcul à des endroits différents dans l'objet.

### _Exercices_

- Réalisez une règle qui permet de changer la couleur d'une face.
- Réalisez une règle qui uniformise la couleur de toute une composante connexe.

-------------------------------------------------------------------------------

## Ecriture de règles complexes

Pour illustrer ces concepts nous vous proposons de faire ensemble la création d'une règle de triangulation barycentrique d'une face. Pour cela regardons un exemple où nous triangulons un quad:
![Triangulation d'un quad](images/obj_triangulation_quad.svg)

Pour concevoir cette opération, il faut vérifier que chaque brin subit la même transformation. Ainsi, le brin rose induit 3 brins roses sur l'objet final et il en est de même avec le brin jaune de l'objet original (et de même pour tous). La suite du travail consiste à observer cette régularité.

![Arcs explicites](images/rule_triangulation_quad_explicit.svg)

![Arcs implicites](images/rule_triangulation_quad_implicit.svg) 

![Calcul plongements](images/rule_triangulation_quad.svg)


#### Exercices
- Réalisez la règle d'extrusion d'une face selon sa normale.
- Réalisez la règle d'extrusion d'une surface selon la normale au sommet. En prenant comme exemple d'application l'objet fourni ``surface_landscape_simple.obj`` dans le répertoire **assets**.
> Le calcul de l'extrusion d'un sommet peut s'écrire de la façon suivante:
> ```
> List<Vec3> ns = <>_normal(face);
>Vec3 n = Vec3::middle(ns);
>n.scale(0.25);
>return face.pos.addConst(n);
> ```
> <span style="color : red;">Attention:}$ vous devez adapter l'orbite pour avoir la bonne normale.

Exemple d'application attendue sur l'objet  de l'asset ``surface_landscape_simple.obj``:

![Surface avant](images/landscape_av.png)
![Surface après](images/landscape_ap.png)

- Réalisez une règle qui permet de relier deux arêtes (alpha2-libre) en ajoutant une face entre les deux. Cette règle plus quelques manipulation de l'outil doivent permettre à partir de la première figure, d'obtenir le second objet:

![Fleche avant](images/fleche_av.png)
![Fleche après](images/fleche_ap.png)

> **Indice:** La dernière règle va contenir deux hooks! chacun devra représenter une arête.

-------------------------------------------------------------------------------
## L'enchainement d'opérations

Jusqu'à présent, les opérations que nous avons créé sont des opérations qui travaillent que sur une orbite de travail. Cependant, il arrive que nous souhaitions généralisées un traitement sur plusieurs orbite voire toute la carte généralisée. Pour cela, Jerboa a définit la notion de _script_ qui ne peuvent pas modifier la topologie mais peut rechercher les endroits où il faut appeler des règles et ainsi enchainé des traitements.

Nous pouvons prendre par exemple l'opération ``jerboa.tuto.triangulation.TriangulationAllFaces`` qui permet de trianguler toutes les faces d'un objets (triangulaire et non-triangulaire) comme le montre la figure ci-après. Pour reproduire ce constat, charger l'objet ``maison_basic.obj`` depuis **File > Load**, sélectionner un brin et appliquer la règle ``TriangulationAllFaces`` du dossier ``triangulation``:

![maison sans triangulation avec face mixte (triangle + quad)](images/maison_av.png) ![maison après triangulation (contient que des triangles)](images/maison_ap.png)

Il aurait été plus judicieux de trianguler que les faces non-triangulaire. Pour cela nous avons donc besoin d'un processus qui va chercher toutes les faces qui ne sont pas des triangles et appeler la règle de triangulation vu [précédemment](#ecriture-de-règles-complexes), c'est le script que nous allons voir ensemble:
- Créer un nouveau script dans l'éditeur
- Dans la zone de code, nous vous fournissons une trame de base qui vous montre comment parcourir toutes la carte généralisée et l'appel à la règle de triangulation d'une face:
```
foreach(JerboaDart dart : gmap) {
    // TODO 
    if(#testSiNonTriangle) {
        @rule<TriangulationFace>(dart);
    }
}
return null;
```
- Vous devez donc remplaçer **#testSiNonTriangle** par le code qui permet de savoir si à partir d'un brin sa face est un triangle ou non.
- Vous pouvez exporter cette opération (premier bouton de la barre de la fenêtre de règle), rafraichir le projet et relancer la compilation.
- Une fois toutes les étapes réalisées avec succès vous pouvez relancer l'exemple ``maison_basic.obj`` et appliquez votre script pour obtenir l'image suivante:
![maison avec une bonne triangulation que des quads](images/maison_ap_correct.png)

#### Exercices

- Réalisez un script qui réalise une extrusion le long d'un chemin (c'est-à-dire, une succession d'extrusion).


